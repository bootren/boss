<%@ page import="com.gitlab.bootren.shiro.context.SessionContextHolder" %>
<%@ page import="com.gitlab.bootren.rbac.remote.AccountService" %>
<%@ page import="com.gitlab.bootren.common.web.SpringContextHolder" %>
<%@ page import="com.gitlab.bootren.rbac.remote.vo.ResourceVo" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>

<%
    if(request.getSession().getAttribute("bossDomain")==null){
        AccountService accountService = SpringContextHolder.getBean(AccountService.class);
        List<ResourceVo> applications = accountService.findApplications();
        for (ResourceVo resourceVo:applications){
            request.getSession().setAttribute(resourceVo.getAppCode() + "Domain", resourceVo.getUrl());
        }
    }
%>


<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <LINK rel="Bookmark" href="${pageContext.request.contextPath}/res/favicon.ico">
    <LINK rel="Shortcut Icon" href="${pageContext.request.contextPath}/res/favicon.ico"/>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="${pageContext.request.contextPath}/res/lib/html5.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/res/lib/respond.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/res/lib/PIE_IE678.js"></script>
    <![endif]-->
    <link href="${pageContext.request.contextPath}/res/css/H-ui.min.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/res/css/H-ui.admin.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/res/lib/icheck/icheck.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/res/lib/jQuery-Validation-Engine-2.6.2-ciaoca/css/validationEngine.jquery.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/res/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/res/lib/iconfont/iconfont.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/res/lib/font-awesome/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <!--[if IE 7]>
    <link href="${pageContext.request.contextPath}/res/lib/font-awesome/font-awesome-ie7.min.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <!--[if IE 6]>
    <script type="text/javascript" src="${pageContext.request.contextPath}/res/lib/DD_belatedPNG_0.0.8a-min.js"></script>
    <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <title>BOSS平台</title>
    <meta name="keywords" content="BOSS平台">
    <meta name="description" content="BOSS平台">
    <script src="${pageContext.request.contextPath}/res/lib/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/res/lib/icheck/jquery.icheck.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/res/lib/jQuery-Validation-Engine-2.6.2-ciaoca/js/jquery.validationEngine-zh_CN.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/res/lib/jQuery-Validation-Engine-2.6.2-ciaoca/js/jquery.validationEngine.min.js"></script>
    <%--<script type="text/javascript" src="${pageContext.request.contextPath}/res/lib/Validform_v5.3.2.js"></script>--%>
    <script type="text/javascript" src="${pageContext.request.contextPath}/res/lib/layer/layer.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/res/js/H-ui.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/res/js/H-ui.admin.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/res/js/H-ui.admin.doc.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/res/js/common.js"></script>
    <script>
        var _domains = {
            bossDomain: '${bossDomain}',
            hrDomain: '${hrDomain}',
            managerDomain: '${managerDomain}',
            universityDomain: '${universityDomain}',
            companyDomain: '${companyDomain}',
            forumDomain: '${forumDomain}'
        };
        layer.config({extend:'skin/moon/style.css'});
        layer.config({
            skin:'layer-ext-moon',
            extend:'skin/moon/style.css'
        });
    </script>
    <sitemesh:write property="head"></sitemesh:write>
</head>
<body>
<header class="Hui-header cl">
    <a class="Hui-logo l" title="H-ui.admin v2.2" href="#">BOSS管理平台</a>
    <a class="Hui-logo-m l" href="/" title="H-ui.admin">BOSS</a>
    <span class="Hui-subtitle l">V1.0</span>
    <span class="Hui-userbox"><span class="c-white">
        <%=SessionContextHolder.getSessionContext().getUser().getName()%>：<%=SessionContextHolder.getSessionContext().getRoles().get(0).getName()%></span>
        <a class="btn btn-danger radius ml-10" href="${pageContext.request.contextPath}/logout" title="退出"><i class="icon-off"></i> 退出</a>
    </span>
    <a aria-hidden="false" class="Hui-nav-toggle" href="${pageContext.request.contextPath}/logout"></a>
</header>
<aside class="Hui-aside">
    <input runat="server" id="divScrollValue" type="hidden" value=""/>

    <div class="menu_dropdown bk_2">
        <dl id="menu-rbac">
            <dt><i class="icon-user"></i> 角色权限<i class="iconfont menu_dropdown-arrow">&#xf02a9;</i></dt>
            <dd>
                <ul>
                    <%--<li><a href="${pageContext.request.contextPath}/rbac/org/list">组织管理</a></li>--%>
                    <%--<li><a href="${pageContext.request.contextPath}/rbac/user/list">用户管理</a></li>--%>
                    <li><a href="${pageContext.request.contextPath}/rbac/role/list">角色授权</a></li>
                    <li><a href="${pageContext.request.contextPath}/rbac/resource/list">资源管理</a></li>
                </ul>
            </dd>
        </dl>
        <dl id="menu-dic">
            <dt><i class="iconfont iconfont-mulu"></i> 数据仓库<i class="iconfont menu_dropdown-arrow">&#xf02a9;</i></dt>
            <dd>
                <ul>
                    <li><a href="${pageContext.request.contextPath}/dic/industry/list">行业库</a></li>
                    <li><a href="${pageContext.request.contextPath}/dic/position/list" href="javascript:void(0)">职位库</a></li>
                    <li><a href="${pageContext.request.contextPath}/dic/area/list" href="javascript:void(0)">区域</a></li>
                    <%--<li><a href="${pageContext.request.contextPath}/dic/company/list" href="javascript:void(0)">企业库</a></li>--%>
                    <li><a href="${pageContext.request.contextPath}/dic/dictionary/list" href="javascript:void(0)">数据字典</a></li>
                </ul>
            </dd>
        </dl>
        <%--<dl id="menu-bis">--%>
        <%--<dt><i class="iconfont iconfont-fasong"></i> 业务管理<i class="iconfont menu_dropdown-arrow">&#xf02a9;</i></dt>--%>
        <%--<dd>--%>
        <%--<ul>--%>
        <%--<li><a href="${pageContext.request.contextPath}/bis/school/list" href="javascript:void(0)">高校接入</a></li>--%>
        <%--<li><a href="${pageContext.request.contextPath}/bis/company/list" href="javascript:void(0)">企业认证</a></li>--%>
        <%--</ul>--%>
        <%--</dd>--%>
        <%--</dl>--%>
        <%--<dl id="menu-module">--%>
        <%--<dt><i class="iconfont iconfont-yingyongzhongxin1"></i> 应用与服务<i class="iconfont menu_dropdown-arrow">&#xf02a9;</i></dt>--%>
        <%--<dd>--%>
        <%--<ul>--%>
        <%--<li><a href="${pageContext.request.contextPath}/module/switching/list" href="javascript:void(0)">数据交换</a></li>--%>
        <%--<li><a href="${pageContext.request.contextPath}/module/sms/config" href="javascript:void(0)">短信服务</a></li>--%>
        <%--<li><a href="${pageContext.request.contextPath}/module/email/config" href="javascript:void(0)">邮件服务</a></li>--%>
        <%--</ul>--%>
        <%--</dd>--%>
        <%--</dl>--%>
        <dl id="menu-security">
            <dt><i class="iconfont iconfont-suoding"></i> 安全中心<i class="iconfont menu_dropdown-arrow">&#xf02a9;</i></dt>
            <dd>
                <ul>
                    <li><a href="${pageContext.request.contextPath}/security/sensitive/list" href="javascript:void(0)">敏感词</a></li>
                    <%--<li><a href="${pageContext.request.contextPath}/security/account/list" href="javascript:void(0)">分类管理</a></li>--%>
                </ul>
            </dd>
        </dl>
        <dl id="menu-security">
            <dt><i class="iconfont iconfont-suoding"></i> 信息统计<i class="iconfont menu_dropdown-arrow">&#xf02a9;</i></dt>
            <dd>
                <ul>
                    <li><a href="${pageContext.request.contextPath}/report/info" href="javascript:void(0)">全局统计</a></li>
                    <li><a href="${pageContext.request.contextPath}/record/list" href="javascript:void(0)">全局日志</a></li>
                </ul>
            </dd>
        </dl>
        <%--<dl id="menu-sysmsg">--%>
        <%--<dt><i class="iconfont iconfont-youjian"></i> 站内消息<i class="iconfont menu_dropdown-arrow">&#xf02a9;</i></dt>--%>
        <%--<dd>--%>
        <%--<ul>--%>
        <%--<li><a href="${pageContext.request.contextPath}/sysmsg/list/send" href="javascript:void(0)">发件箱</a></li>--%>
        <%--<li><a href="${pageContext.request.contextPath}/sysmsg/list/receive" href="javascript:void(0)">收件箱</a></li>--%>
        <%--</ul>--%>
        <%--</dd>--%>
        <%--</dl>--%>
        <%--<dl id="menu-notice">--%>
        <%--<dt><i class="iconfont iconfont-yangshengqi"></i> 公告管理<i class="iconfont menu_dropdown-arrow">&#xf02a9;</i></dt>--%>
        <%--<dd>--%>
        <%--<ul>--%>
        <%--<li><a href="${pageContext.request.contextPath}/notice/list">公告管理</a></li>--%>
        <%--</ul>--%>
        <%--</dd>--%>
        <%--</dl>--%>
        <%--<dl id="menu-service">--%>
        <%--<dt><i class="iconfont iconfont-erji"></i> 客服中心<i class="iconfont menu_dropdown-arrow">&#xf02a9;</i></dt>--%>
        <%--<dd>--%>
        <%--<ul>--%>
        <%--<li><a href="${pageContext.request.contextPath}/suggestion/list" href="javascript:void(0)">投诉建议</a></li>--%>
        <%--<li><a href="${pageContext.request.contextPath}/report/found" href="javascript:void(0)">已处理举报</a></li>--%>
        <%--<li><a href="${pageContext.request.contextPath}/report/find" href="javascript:void(0)">待处理举报</a></li>--%>
        <%--</ul>--%>
        <%--</dd>--%>
        <%--</dl>--%>
        <%--<dl id="menu-sys">--%>
        <%--<dt><i class="icon-cogs"></i> 系统管理<i class="iconfont menu_dropdown-arrow">&#xf02a9;</i></dt>--%>
        <%--<dd>--%>
        <%--<ul>--%>
        <%--<li><a href="${pageContext.request.contextPath}/sys/setting" href="javascript:void(0)">基本设置</a></li>--%>
        <%--</ul>--%>
        <%--</dd>--%>
        <%--</dl>--%>
    </div>
</aside>
<div class="dislpayArrow"><a class="pngfix" href="javascript:void(0);" onClick="displaynavbar(this)"></a></div>
<section class="Hui-article-box">
    <sitemesh:write property="body"></sitemesh:write>
</section>
<%--<a href="javascript:void(0)" class="iconfont toTop" title="返回顶部" alt="返回顶部" style="display: none;">󰀣</a>--%>
<script>
    $(function () {
        var url = location.pathname;
        var currentMenuDl = $("a[href='" + url + "']").closest("dl").find("dt").click();
    });
</script>
</body>
</html>