/*!
 * 地区选择
 * @author LiYonghui
 * @version 1.0
 * @require jQuery
 * 2015.7.16
 */


(function (win, udf) {
    'use strict';

    // 保存实例化对象的data键
    var datakey = 'jquery-area',
    // jquery对象 或 Jquery对象
        $ = win.$,
    // 默认参数
        defaults = {
            //dataUrl: _domains["bossDomain"] + "/rest/dict/area",//数据源
            single: true,//是否为单选
            inputHeight: 35,//结果容器高度
            inputWidth: 'auto',//结果容器高度
            replaceEl: true,//是否替换元素
            selectorId: "__area_selector",
            onBeforeShow: $.noop,
            onAfterHide: $.noop,
            callback: $.noop
        };


    // 原型方法，驼峰形式
    $.fn.area = function (settings) {
        // 当前第1个参数为字符串
        var run = $.type(settings) === 'string',
        // 获取运行方法时的其他参数
            args = [].slice.call(arguments, 1),
        // 复制默认配置
            options = $.extend({}, defaults),
        // 运行实例化方法的元素
            $element,
        // 实例化对象
            instance;

        // 运行实例化方法，第1个字符不能是“_”
        // 下划线开始的方法皆为私有方法
        if (run && run[0] !== '_') {
            if (!this.length) return;

            // 只取集合中的第1个元素
            $element = $(this[0]);

            // 获取保存的实例化对象
            instance = $element.data(datakey);

            // 若未保存实例化对象，则先保存实例化对象
            if (!instance) $element.data(datakey, instance = new Constructor($element[0], options)._init());

            // 防止与静态方法重合，只运行原型上的方法
            // 返回原型方法结果，否则返回undefined
            return Constructor.prototype[settings] ? Constructor.prototype[settings].apply(instance, args) : udf;
        }
        // instantiation options
        else if (!run) {
            // 合并参数
            options = $.extend(options, settings);
        }

        return this.each(function () {
            var element = this,
                instance = $(element).data(datakey);

            // 如果没有保存实例
            if (!instance) {
                // 保存实例
                $(element).data(datakey, instance = new Constructor(element, options)._init());
            }
        });
    };

    // 暴露插件的默认配置
    $.fn.area.defaults = defaults;


    // 构造函数

    function Constructor(element, options) {
        var the = this;
        the.$element = $(element);
        the.options = options;
        this.resultData = [];
        this.cacheItem = null;
    }


    // 原型方法，驼峰写法
    Constructor.prototype = {
        /**
         * 初始化
         * @return this
         */
        _init: function () {
            // 初始化
            this._fetchData();
            return this;
        },


        // 其他私有原型方法
        /**
         * 获取基本数据
         * @private
         */
        _fetchData: function () {
            this.data = AreaData;
            this._buildContainer();
            this._buildSelector();
        },
        _buildContainer: function () {
            if (this.options.replaceEl == true) {
                this.$element.hide().wrap('<div class="xsr-area-input"></div>');
                this.$input = this.$element.parent(".xsr-area-input").css({
                    height: this.options.inputHeight,
                    width: this.options.inputWidth
                });
            } else {
                this.$input = this.$element.addClass("xsr-area-input");
            }
            var _this = this;
            this.$input.unbind().on("click", function () {
                _this.open();
            });
        },
        _buildSelector: function () {
            var _this = this;
            var _id = this.options.selectorId;
            if ($("#" + _id).length == 0) {
                var _selector = $('<div id="' + _id + '" class="xsr-area-selector box" style="display:none;"></div>');
                var _panel = $('<div class="xsr-area-panel"><table><tr><td class="turn-left"><a>&lt;</a></td><td id="area-center" class="area-center">' +
                    '<div id="area-data-1" data-index="1" style="display: none;"></div><div id="area-data-2" data-index="2" style="display: none;"></div><div id="area-data-3" data-index="3" style="display: none;"></div>' +
                    '</td><td class="turn-right"><a>&gt;</a></td></tr></table></div>');
                var _result = $('<div class="xsr-area-result"><table style="width:100%;table-layout: fixed;"><tr><td style="width:50px;">已选：</td><td id="area-result-td"></td><td style="width: 80px;"><a href="javascript:void(0);" id="area-ok">确定</a><a  href="javascript:void(0);" id="area-cancel">取消</a></td></tr></table></div>');
                _selector.append(_result).append(_panel).appendTo($('body'));
            }
            this.$selector = $("#" + _id);

            this._setData("province");

            this.$selector.find("#area-ok").on("click", function () {
                _this.$selector.find(".active").removeClass("active");
                _this.$selector.find("#area-center div:eq(0)").show().siblings().hide();
                _this._pushResult();
                _this.close();
            });
            this.$selector.find("#area-cancel").on("click", function () {
                _this.close();
            });

            this.$selector.find(".turn-left a,.turn-right a").click(function () {

                var $this = $(this), currentPanel = $("#area-center > div:visible"), visibleIndex = currentPanel.attr("data-index");
                var targetIndex = visibleIndex, canTurn = true;
                if ($this.parent().is(".turn-left")) {
                    if (visibleIndex > 1) {
                        targetIndex = parseInt(visibleIndex) - 1;
                    }
                } else {
                    if (currentPanel.find("a.active").length == 0) {
                        canTurn = false;
                    }
                    if (visibleIndex < 3) {
                        targetIndex = parseInt(visibleIndex) + 1;
                    }
                }
                if (canTurn) {
                    $("#area-center > div").hide();
                    $("#area-center > div[data-index=" + targetIndex + "]").show();
                }
            });

            //地区点击事件
            this.$selector.find("#area-center").delegate("a", "click", function () {
                var $this = $(this);
                var type = $this.attr("data-area-type"), _id = $this.attr("data-id"), _text = $this.text();
                if (_this.cacheItem == undefined) {
                    _this.cacheItem = {};
                }
                _this.cacheItem.type = type;
                _this.cacheItem.id = _id;
                _this.cacheItem.name = _text;
                //点击省份
                if (type == 1) {
                    _this.cacheItem.province = _text;
                    _this.cacheItem.provinceId = _id;
                    _this.cacheItem.city = null;
                    _this.cacheItem.cityId = null;
                    _this.cacheItem.county = null;
                    _this.cacheItem.countyId = null;
                }
                if (type == 2) {
                    _this.cacheItem.city = _text;
                    _this.cacheItem.cityId = _id;
                    _this.cacheItem.county = null;
                    _this.cacheItem.countyId = null;
                }
                if (type == 3) {
                    _this.cacheItem.county = _text;
                    _this.cacheItem.countyId = _id;
                }

                _this._setResultView();

                $this.addClass("active").siblings().removeClass("active");
                if ($this.attr("data-child") != undefined) {
                    $this.closest("div").hide();
                    _this._setData($this.attr("data-child"), $this.attr("data-id"));
                }
            });
        },

        _setResultView: function () {
            if (this.cacheItem != undefined && this.cacheItem != null) {
                var html = "";
                html += (this.cacheItem.province != null ? this.cacheItem.province : "");
                html += (this.cacheItem.city != null ? "-" + this.cacheItem.city : "");
                html += (this.cacheItem.county != null ? "-" + this.cacheItem.county : "");
                this.cacheItem.name = html;
                $("#area-result-td").empty().append(html);
            } else {
                $("#area-result-td").empty();
            }
        },
        _setData: function (type, id) {
            var _this = this;
            switch (type) {
                case "city":
                    if (!id) {
                        if (window.layer != undefined) {
                            layer.msg("必须先选择一个省份！", {icon: 5});
                        } else {
                            alert("必须先选择一个省份！");
                        }
                    }
                case "county":
                    if (!id) {
                        if (window.layer != undefined) {
                            layer.msg("必须先选择一个城市！", {icon: 5});
                        } else {
                            alert("必须先选择一个城市！");
                        }
                    }
            }
            var _province = "", _city = "", _county = "";
            $(this.data).each(function (i, item) {
                //查询省份
                if (type == "province" && item.areaType == 1) {
                    _province += '<a href="javascript:void(0);" data-id="' + item.id + '" data-child="city" data-area-type="1" data-parent-id="' + item.parentId + '">' + item.name + '</a>';
                }
                if (type == "city" && item.areaType == 2 && item.parentId == id) {
                    _city += '<a href="javascript:void(0);" data-id="' + item.id + '" data-child="county" data-area-type="2" data-parent-id="' + item.parentId + '">' + item.name + '</a>';
                }
                if (type == "county" && item.areaType == 3 && item.parentId == id) {
                    _county += '<a href="javascript:void(0);" data-id="' + item.id + '" data-area-type="3" data-parent-id="' + item.parentId + '">' + item.name + '</a>';
                }
            });
            if (type == "province") {
                this.$selector.find("#area-data-1").empty().append(_province).show();
            }
            if (type == "city") {
                this.$selector.find("#area-data-2").empty().append(_city).show();
            }
            if (type == "county") {
                this.$selector.find("#area-data-3").empty().append(_county).show();
            }
        },

        _arrRemove: function (arrObj, objProperty, objValue) {
            return $.grep(arrObj, function (cur, i) {
                return cur[objProperty] != objValue;
            });
        },
        _pushResult: function () {
            if (this.options.single == true) {
                this.resultData = [];
                this.resultData.push(this.cacheItem);
            } else {
                if (this.cacheItem != null && this.cacheItem != undefined) {
                    //移除重复
                    this.resultData = this._arrRemove(this.resultData, "id", this.cacheItem.id);
                    this.resultData.push(this.cacheItem);
                }
            }
            this.cacheItem = null;
            this._setResultView();
            this._fillInput();
        },
        _getData: function () {
            return this.resultData;
        },
        /**
         * 填充结果到容器
         * @private
         */
        _fillInput: function () {
            var _this = this, container = this.$input;
            this.$element.siblings().remove();
            if (this.resultData.length > 0) {
                $(this.resultData).each(function (i, item) {
                    var dataItem = item || null;
                    if (dataItem != null) {
                        var row = $('<span class="xsr-area-input-item">' + dataItem.name + '</span>')
                            .append($('<a href="javascript:void(0);">x</a>').click(function () {
                                _this.resultData = _this._arrRemove(_this.resultData, "id", dataItem.id);
                                row.remove();
                                return false;
                            }));
                        container.append(row);
                    }
                });
                this.options.callback(this.resultData);
            }
        },

        /**
         * 设置或获取选项
         * @param  {String/Object} key 键或键值对
         * @param  {*}             val 值
         * @return 获取时返回键值，否则返回this
         */
        options: function (key, val) {
            // get
            if ($.type(key) === 'string' && val === udf) return this.options[key];

            var map = {};
            if ($.type(key) === 'object') map = key;
            else map[key] = val;

            this.options = $.extend(this.options, map);
        },


        // 其他公有原型方法
        /**
         * 打开弹窗
         */
        open: function () {
            this.$selector.show();
            this.setPosition();
        }
        ,
        /**
         * 关闭弹窗
         */
        close: function () {
            this.$selector.hide();
        },
        setPosition: function () {
            this.$selector.offset({left: this.$input.offset().left, top: this.$input.offset().top + this.options.inputHeight});
        },
        getData: function () {
            return this.resultData;
        }
    };
})
(this);
