/*!
 * 地区选择
 * @author cc
 * @version 1.0
 * @require jQuery
 * 2015.7.17
 */


(function (win, udf) {
    'use strict';

    // 保存实例化对象的data键
    var datakey = 'jquery-position',
    // jquery对象 或 Yquery对象
        $ = win.$,
    // 默认参数
        defaults = {
            single: true,//是否为单选
            inputHeight: 35,//结果容器高度
            inputWidth: 'auto',//结果容器宽度
            industry1: "",
            industry2: "",
            selectorId: "__position_selector",
            onBeforeShow: $.noop,
            onAfterHide: $.noop,
            callback: $.noop
        };


    // 原型方法，驼峰形式
    $.fn.positions = function (settings) {
        // 当前第1个参数为字符串
        var run = $.type(settings) === 'string',
        // 获取运行方法时的其他参数
            args = [].slice.call(arguments, 1),
        // 复制默认配置
            options = $.extend({}, defaults),
        // 运行实例化方法的元素
            $element,
        // 实例化对象
            instance;

        // 运行实例化方法，第1个字符不能是“_”
        // 下划线开始的方法皆为私有方法
        if (run && run[0] !== '_') {
            if (!this.length) return;

            // 只取集合中的第1个元素
            $element = $(this[0]);

            // 获取保存的实例化对象
            instance = $element.data(datakey);

            // 若未保存实例化对象，则先保存实例化对象
            if (!instance) $element.data(datakey, instance = new Constructor($element[0], options)._init());

            // 防止与静态方法重合，只运行原型上的方法
            // 返回原型方法结果，否则返回undefined
            return Constructor.prototype[settings] ? Constructor.prototype[settings].apply(instance, args) : udf;
        }
        // instantiation options
        else if (!run) {
            // 合并参数
            options = $.extend(options, settings);
        }

        return this.each(function () {
            var element = this,
                instance = $(element).data(datakey);

            // 如果没有保存实例
            if (!instance) {
                // 保存实例
                $(element).data(datakey, instance = new Constructor(element, options)._init());
            }
        });
    };

    // 暴露插件的默认配置
    $.fn.positions.defaults = defaults;


    // 构造函数

    function Constructor(element, options) {
        var the = this;
        the.$element = $(element);
        the.options = options;
        this.resultData = [];
        this.cacheItem = null;
    }


    // 原型方法，驼峰写法
    Constructor.prototype = {
        /**
         * 初始化
         * @return this
         */
        _init: function () {
            // 初始化
            this._fetchData();
            return this;
        },


        // 其他私有原型方法
        /**
         * 获取基本数据
         * @private
         */
        _fetchData: function () {
            //JS数据源
            this.data = category_zh_CN;
            this._buildContainer();
            this._buildSelector();
        },
        _buildContainer: function () {
            if (this.options.replaceEl == true) {
                this.$element.hide().wrap('<div class="xsr-position-input"></div>');
                this.$input = this.$element.parent(".xsr-position-input").css({
                    height: this.options.inputHeight,
                    width: this.options.inputWidth
                });
            } else {
                this.$input = this.$element.addClass("xsr-position-input");
            }
            var _this = this;
            this.$input.unbind().on("click", function () {
                _this.open();
            });
        },
        _buildSelector: function () {
            var _this = this;
            var _id = this.options.selectorId;
            if ($("#" + _id).length == 0) {
                var _selector = $('<div id="' + _id + '" class="xsr-position-selector box" style="display:none;"></div>');
                var _panel = $('<div class="xsr-position-panel"><table><tr><td class="turn-left"><a>&lt;</a></td><td id="position-center" class="position-center">' +
                    '<div id="position-data-1" data-index="1" style="display: none;"></div><div id="position-data-2" data-index="2" style="display: none;"></div><div id="position-data-3" data-index="3" style="display: none;"></div>' +
                    '</td><td class="turn-right"><a>&gt;</a></td></tr></table></div>');
                var _result = $('<div class="xsr-position-result"><table style="width:100%;table-layout: fixed;"><tr><td style="width:50px;">已选：</td><td id="position-result-td"></td><td style="width: 80px;"><a href="javascript:void(0);" id="position-ok">确定</a><a  href="javascript:void(0);" id="position-cancel">取消</a></td></tr></table></div>');
                var _industry = $('<div class="xsr-industry-select">请选择行业：</div>');

                this.in2 = $('<select id="industry2select"></select>').on("change", function () {
                    var c2 = $(this).find("option:selected").val();
                    _this.industry2 = c2;
                    _this._setData("first");
                });
                this.in1 = $('<select id="industry1select"></select>').append('<option>请选择行业大类</option>').on("change", function () {
                    var c1 = $(this).find("option:selected").val();
                    _this._changeIn1(c1);
                });
                $(this.data).each(function (i, item) {
                    if (item.type == "industry") {
                        _this.in1.append('<option value="' + item.code + '">' + item.name + '</option>');
                    }
                });
                _industry.append(this.in1).append(this.in2);
                _selector.append(_result).append(_industry).append(_panel).appendTo($('body'));
            }
            this.$selector = $("#" + _id);

            //this._setData("first");

            this.$selector.find("#position-ok").on("click", function () {
                _this.$selector.find(".active").removeClass("active");
                _this.$selector.find("#position-center div:eq(0)").show().siblings().hide();
                _this._pushResult();
                _this.close();
            });
            this.$selector.find("#position-cancel").on("click", function () {
                _this.close();
            });

            this.$selector.find(".turn-left a,.turn-right a").click(function () {

                var $this = $(this), currentPanel = $("#position-center > div:visible"), visibleIndex = currentPanel.attr("data-index");
                var targetIndex = visibleIndex, canTurn = true;
                if ($this.parent().is(".turn-left")) {
                    if (visibleIndex > 1) {
                        targetIndex = parseInt(visibleIndex) - 1;
                    }
                } else {
                    if (currentPanel.find("a.active").length == 0) {
                        canTurn = false;
                    }
                    if (visibleIndex < 3) {
                        targetIndex = parseInt(visibleIndex) + 1;
                    }
                }
                if (canTurn) {
                    $("#position-center > div").hide();
                    $("#position-center > div[data-index=" + targetIndex + "]").show();
                }
            });

            //点击事件
            this.$selector.find("#position-center").delegate("a", "click", function () {
                var $this = $(this);
                var type = $this.attr("data-position-type"), _id = $this.attr("data-code"), _text = $this.text();
                if (_this.cacheItem == undefined) {
                    _this.cacheItem = {};
                }
                _this.cacheItem.type = type;
                _this.cacheItem.id = _id;
                _this.cacheItem.name = _text;
                //点击
                if (type == 1) {
                    _this.cacheItem.first = _text;
                    _this.cacheItem.firstId = _id;
                    _this.cacheItem.second = null;
                    _this.cacheItem.secondId = null;
                    _this.cacheItem.third = null;
                    _this.cacheItem.thirdId = null;
                }
                if (type == 2) {
                    _this.cacheItem.second = _text;
                    _this.cacheItem.secondId = _id;
                    _this.cacheItem.third = null;
                    _this.cacheItem.thirdId = null;
                }
                if (type == 3) {
                    _this.cacheItem.third = _text;
                    _this.cacheItem.thirdId = _id;
                }

                _this._setResultView();

                $this.addClass("active").siblings().removeClass("active");
                if ($this.attr("data-child") != undefined) {
                    $this.closest("div").hide();
                    _this._setData($this.attr("data-child"), $this.attr("data-code"));
                }
            });
        },
        _setIn1: function (v, v2, callback) {
            this.in1.find("option[value='" + v + "']").prop("selected", true);
            this.industry1 = v;
            this._changeIn1(v, v2, callback);
        },
        _changeIn1: function (v1, v2, callback) {
            var _this = this;
            _this.industry1 = v1;
            if (v2) {
                _this.industry2 = v2;
            }
            _this.in2.empty().append("<option>请选择行业小类</option>");

            $(_this.data).each(function (i, p) {
                if (p.code == _this.industry1) {
                    $(p.children).each(function (j, item) {
                        if (item.type == "industry") {
                            _this.in2.append('<option ' + (v2 ? 'selected="selected"' : '') + ' value="' + item.code + '">' + item.name + '</option>');
                        }
                    });
                }
            });
            if (_this.industry2) {
                _this._setData("first");
            }
        },

        _setResultView: function () {
            if (this.cacheItem != undefined && this.cacheItem != null) {
                var html = "";
                html += (this.cacheItem.first != null ? this.cacheItem.first : "");
                html += (this.cacheItem.second != null ? "-" + this.cacheItem.second : "");
                html += (this.cacheItem.third != null ? "-" + this.cacheItem.third : "");
                $("#position-result-td").empty().append(html);
            } else {
                $("#position-result-td").empty();
            }
        },
        _setData: function (type, code) {
            var _this = this;

            switch (type) {
                case "second":
                    if (!code) {
                        if (window.layer != undefined) {
                            layer.msg("必须先选择一个职位！", {icon: 5});
                        } else {
                            alert("必须先选择一个职位！");
                        }
                    }
                case "third":
                    if (!code) {
                        if (window.layer != undefined) {
                            layer.msg("必须先选择一个职位！", {icon: 5});
                        } else {
                            alert("必须先选择一个职位！");
                        }
                    }
            }
            var _first = "", _second = "", _third = "";
            if (type == "first") {
                var obj;
                $(this.data).each(function (i, item) {
                    if (item.code == _this.industry1) {
                        obj = item.children;
                        return false;
                    }
                });
                $(obj).each(function (i, item) {
                    if (item.code == _this.industry2) {
                        obj = item.children;
                        return false;
                    }
                });
                _this.tmpPosition1Array = obj;
                $(obj).each(function (i, item) {
                    if (item.type == "position") {
                        _first += '<a href="javascript:void(0);" data-code="' + item.code + '" data-child="second" data-position-type="1">' + item.name + '</a>';
                    }
                });
            }
            if (type == "second") {
                var obj = _this.tmpPosition1Array;
                $(obj).each(function (i, item) {
                    if (item.code == code) {
                        obj = item.children;
                        return false;
                    }
                });
                _this.tmpPosition2Array = obj;
                $(obj).each(function (i, item) {
                    if (item.type == "position") {
                        if (item.children) {
                            _second += '<a href="javascript:void(0);" data-code="' + item.code + '" data-child="third" data-position-type="2">' + item.name + '</a>';
                        } else {
                            _second += '<a href="javascript:void(0);" data-code="' + item.code + '"  data-position-type="2">' + item.name + '</a>';
                        }
                    }
                });
            }
            if (type == "third") {
                var obj = _this.tmpPosition2Array;
                $(obj).each(function (i, item) {
                    if (item.code == code) {
                        obj = item.children;
                        return false;
                    }
                });
                _this.tmpPosition3Array = obj;
                $(obj).each(function (i, item) {
                    if (item.type == "position") {
                        _third += '<a href="javascript:void(0);" data-code="' + item.code + '" data-position-type="3">' + item.name + '</a>';
                    }
                });
            }


            //$(this.data).each(function (i, item) {
            //    //查询
            //    if (type == "first" && item.parentId == null) {
            //        _first += '<a href="javascript:void(0);" data-code="' + item.id + '" data-child="second" data-position-type="1" data-parent-id="' + item.parentId + '">' + item.name + '</a>';
            //    }
            //    if (type == "second" && item.parentId != null && item.parentId == id) {
            //        _second += '<a href="javascript:void(0);" data-code="' + item.id + '" data-child="third" data-position-type="2" data-parent-id="' + item.parentId + '">' + item.name + '</a>';
            //    }
            //    if (type == "third" && item.parentId != null && item.parentId == id) {
            //        _third += '<a href="javascript:void(0);" data-code="' + item.id + '" data-position-type="3" data-parent-id="' + item.parentId + '">' + item.name + '</a>';
            //    }
            //});
            //

            if (type == "first") {
                this.$selector.find("#position-data-1").empty().append(_first).show();
                this.$selector.find("#position-data-2").empty();
                this.$selector.find("#position-data-3").empty();
            }
            if (type == "second") {
                this.$selector.find("#position-data-2").empty().append(_second).show();
                this.$selector.find("#position-data-3").empty();
            }
            if (type == "third") {
                this.$selector.find("#position-data-3").empty().append(_third).show();
            }
        },

        _arrRemove: function (arrObj, objProperty, objValue) {
            return $.grep(arrObj, function (cur, i) {
                return cur[objProperty] != objValue;
            });
        },
        _pushResult: function () {
            if (this.cacheItem != null && this.cacheItem != undefined) {
                //移除重复
                this.resultData = [];//清空数组，单选
                this.resultData = this._arrRemove(this.resultData, "id", this.cacheItem.id);

                this.resultData.push(this.cacheItem);
            }
            this.cacheItem = null;
            this._setResultView();
            this._fillInput();
        },
        _getData: function () {
            return this.resultData;
        },
        /**
         * 填充结果到容器
         * @private
         */
        _fillInput: function () {
            var _this = this, container = this.$input;
            this.$element.siblings().remove();
            if (this.resultData.length > 0) {
                $(this.resultData).each(function (i, item) {
                    var dataItem = item || null;
                    if (dataItem != null) {
                        var row = $('<span class="xsr-position-input-item">' + dataItem.name + '</span>')
                            .append($('<a href="javascript:void(0);">x</a>').click(function () {
                                _this.resultData = _this._arrRemove(_this.resultData, "id", dataItem.id);
                                row.remove();
                                return false;
                            }));
                        container.append(row);
                    }
                });
                this.options.callback(this.resultData);
            }
        },

        /**
         * 设置或获取选项
         * @param  {String/Object} key 键或键值对
         * @param  {*}             val 值
         * @return 获取时返回键值，否则返回this
         */
        options: function (key, val) {
            // get
            if ($.type(key) === 'string' && val === udf) return this.options[key];

            var map = {};
            if ($.type(key) === 'object') map = key;
            else map[key] = val;

            this.options = $.extend(this.options, map);
        },


        // 其他公有原型方法
        setIndustry: function (industry1, industry2) {
            var _this = this;
            if (industry1) {
                var i1;
                $(_this.data).each(function (i, item) {
                    if (item.name == industry1) {
                        industry1 = item.code;
                        i1 = item;
                        return false;
                    }
                });
                if (industry2 && i1 && i1.children) {
                    $(i1.children).each(function (i, item) {
                        if (item.name == industry2) {
                            industry2 = item.code;
                            return false;
                        }
                    });
                }
                _this._setIn1(industry1, industry2);
            }
        },

        /**
         * 打开弹窗
         */
        open: function () {
            this.$selector.show();
            this.setPosition();
        },
        /**
         * 关闭弹窗
         */
        close: function () {
            this.$selector.hide();
        },
        setPosition: function () {
            this.$selector.offset({
                //left: this.$input.offset().left,
                left: $(window).width() / 2 - 400,
                top: this.$input.offset().top + this.options.inputHeight
            });
        },
        getData: function () {
            return this.resultData;
        }
    };
})
(this);
