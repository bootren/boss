//放入common.jsp  这里只作为源
/**
 * sso情况验证
 * @param loginFunc  已登录回调  参数为正常返回的参数
 * @param noLoginFunc
 * @private
 */
function __isLogin(loginFunc, noLoginFunc) {
    try {
        $.ajax({
            type: "get", cache: false, dataType: "jsonp", crossDomain: true, url: _domains["contextDomain"] + "/sessionVariable?type=user&__t=" + Math.random() + "&callback=?",
            success: function (data) {
                if (loginFunc != undefined && typeof (loginFunc) == "function") {
                    loginFunc(data);
                }
            },
            error: function (error) {
                if (noLoginFunc != undefined && typeof (noLoginFunc) == "function") {
                    noLoginFunc();
                }
            }
        });
    } catch (e) {
    }
}