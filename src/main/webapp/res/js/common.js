
//分页工具支持（处理请求与响应数据的转换）
var pageUtil = {
    responseHandler: function (response) {
        var page = {};
        page.rows = response.content || [];
        page.current = response.number + 1;
        page.rowCount = response.size;
        page.total = response.totalElements || 0;
        return page;
    },
    requestHandler: function (request) {
        request.page = request.current - 1;
        request.size = request.rowCount;
        return request;
    }
};
