<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>首页</title>
</head>
<body>
<nav class="breadcrumb">
    <i class="iconfont">󰄫</i> <span class="c-666">首页</span>
</nav>
<div class="Hui-article">
    <article class="cl pd-20">
        <p class="f-20 text-success">欢迎使用BOSS平台 <span class="f-14">V1.0</span>！</p>
        <table class="table table-border table-bordered table-bg mt-20">
            <thead>
            <tr>
                <th colspan="2" scope="col">参数展示</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="item" items="${sys}">
                <tr>
                    <th width="200"></th>
                    <td><span>${item}</span></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </article>
</div>

</body>
</html>