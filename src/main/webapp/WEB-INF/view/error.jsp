<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<%
    int status = Integer.valueOf(request.getAttribute("status").toString());
    response.setStatus(status);
    request.getRequestDispatcher(status+".jsp").forward(request,response);
%>
</body>
</html>
