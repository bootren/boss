<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
  <title>首页</title>
</head>
<body>
<nav class="breadcrumb">
  <i class="iconfont">󰄫</i> <a class="" href="${pageContext.request.contextPath}/">首页</a>
  <span class="c-999 en">&gt;</span><span class="c-666">企业认证管理</span>
  <span class="c-gray en">&gt;</span> <c:if test="${empty model}">新增</c:if><c:if test="${not empty model}">编辑</c:if>企业认证
  <a class="btn btn-success radius r mr-20"
     style="line-height:1.6em;margin-top:3px" href="${pageContext.request.contextPath}/bis/company/list" title="返回">
    <i class="icon-reply"></i>
  </a>
</nav>
<div class="Hui-article">
  <article class="cl pl-20 pr-20 pt-10 pb-10">
    <form action="" method="post" class="form form-horizontal" id="form-company-add">
      <input type="hidden" name="companyCode" value="${model.companyCode}"/>
      <div class="row cl">
        <label class="form-label col-2"><span class="c-red">*</span>公司代码：</label>

        <div class="formControls col-5">
          <input type="text" class="input-text" data-validation-engine="validate[required,maxSize[30]]" value="${model.state}" placeholder="" id="state" name="state">
        </div>
        <div class="col-5"></div>
      </div>
      <div class="row cl">
        <label class="form-label col-2"><span class="c-red">*</span>认证状态：</label>

        <div class="formControls col-5">
          <input type="text" class="input-text" data-validation-engine="validate[required,maxSize[30]]" value="${model.companyCode}" placeholder="" id="companyCode" name="companyCode">
        </div>
        <div class="col-5"></div>
      </div>
      <div class="row cl">
        <label class="form-label col-2"><span class="c-red">*</span>审核人员：</label>

        <div class="formControls col-5">
          <input type="text" class="input-text" data-validation-engine="validate[required,maxSize[30]]" value="${model.operator}" placeholder="" id="operator" name="operator">
        </div>
        <div class="col-5"></div>
      </div>
      <div class="row cl">
        <label class="form-label col-2"><span class="c-red">*</span>处理时间：</label>

        <div class="formControls col-5">
          <input type="text" class="input-text" data-validation-engine="validate[required,maxSize[30]]" value="${model.operteTime}" placeholder="" id="operteTime" name="operteTime">
        </div>
        <div class="col-5"></div>
      </div>
      <div class="row cl">
        <label class="form-label col-2"><span class="c-red">*</span>审核意见：</label>

        <div class="formControls col-5">
          <input type="text" class="input-text" data-validation-engine="validate[required,maxSize[30]]" value="${model.remark}" placeholder="" id="remark" name="remark">
        </div>
        <div class="col-5"></div>
      </div>

      <div class="row cl">
        <div class="col-7 col-offset-4">
          <input class="btn btn-primary radius" type="button" onclick="formSubmit();" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
          <input class="btn radius" type="button" onclick="location='${pageContext.request.contextPath}/bis/company/list'" value="&nbsp;&nbsp;返回&nbsp;&nbsp;">
        </div>
      </div>
    </form>
  </article>
</div>

<!-- Scripts -->
<script>
  $(function () {
    $('#form-company-add').validationEngine({promptPosition: "centerRight"});
  });

  function formSubmit() {
    if ($('#form-company-add').validationEngine("validate")) {
      $.ajax({
        url: '${pageContext.request.contextPath}/bis/company/save',
        type: 'post',
        data: $("#form-company-add").serialize(),
        async: false,
        cache: false,
        success: function (data) {
          if (data.ok == true) {
            layer.msg('保存成功!');
            window.location = '${pageContext.request.contextPath}/bis/company/list';
          } else {
            layer.msg('保存失败!');
          }
        },
        error: function () {
          layer.msg('保存失败!');
        }
      })
    }
  }

</script>
</body>
</html>