<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>首页</title>
</head>
<body>
<nav class="breadcrumb">
    <i class="iconfont">󰄫</i> <a class="" href="${pageContext.request.contextPath}/">首页</a>
    <span class="c-gray en">&gt;</span> <a href="${pageContext.request.contextPath}/rbac/role/list">用户管理</a>
    <span class="c-gray en">&gt;</span> <c:if test="${empty model}">新增</c:if><c:if test="${not empty model}">编辑</c:if>用户
    <a class="btn btn-success radius r mr-20"
       style="line-height:1.6em;margin-top:3px" href="${pageContext.request.contextPath}/rbac/role/list" title="返回">
        <i class="icon-reply"></i>
    </a>
</nav>
<div class="Hui-article">
    <article class="cl pl-20 pr-20 pt-10 pb-10">
        <form action="" method="post" class="form form-horizontal" id="form-add">
            <input type="hidden" name="id" value="${model.id}"/>
            <%--<input type="hidden" name="createTime" value="${model.createTime}"/>--%>
            <%--<input type="hidden" name="creator" value="${model.creator}"/>--%>

            <div class="row cl">
                <label class="form-label col-2">编号：</label>

                <div class="formControls col-5">
                    <c:choose>
                        <c:when test="${not empty model}">
                            <input type="text" class="input-text disabled" value="${model.code}" readonly="readonly"
                                   id="code" name="code">
                        </c:when>
                        <c:otherwise>
                            <input type="text" class="input-text" id="code" value="${model.code}" data-validation-engine="validate[required,maxSize[128]]" name="code">
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="col-5"></div>
            </div>
            <div class="row cl">
                <label class="form-label col-2"><span class="c-red">*</span>名称：</label>

                <div class="formControls col-5">
                    <input type="text" class="input-text" data-validation-engine="validate[required,maxSize[30]]" value="${model.name}" placeholder="" id="name" name="name">
                </div>
                <div class="col-5"></div>
            </div>
            <div class="row cl">
                <label class="form-label col-2"><span class="c-red"></span>权限value：</label>

                <div class="formControls col-5">
                    <input type="text" class="input-text" data-validation-engine="validate[maxSize[256]]" value="${model.value}" placeholder="/account/user/view/**" id="value" name="value">
                </div>
                <div class="col-5"></div>
            </div>

            <div class="row cl">
                <div class="col-7 col-offset-4">
                    <input class="btn btn-primary radius" type="button" onclick="formSubmit();" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
                    <input class="btn radius" type="button" onclick="location='${pageContext.request.contextPath}/rbac/role/list'" value="&nbsp;&nbsp;返回&nbsp;&nbsp;">
                </div>
            </div>
        </form>
    </article>
</div>

<!-- Scripts -->
<script>
    $(function () {
        $('#form-add').validationEngine({promptPosition: "centerRight"});

        $('.skin-minimal input').iCheck({
            checkboxClass: 'icheckbox-blue',
            radioClass: 'iradio-blue',
            increaseArea: '20%'
        });
    });

    function formSubmit() {
        if ($('#form-add').validationEngine("validate")) {
            $.ajax({
                url: '${pageContext.request.contextPath}/rbac/role/save',
                type: 'post',
                data: $("#form-add").serialize(),
                async: false,
                cache: false,
                success: function (data) {
                    if (data.ok == true) {
                        layer.msg('保存成功!');
                        window.location = '${pageContext.request.contextPath}/rbac/role/list';
                    } else {
                        layer.msg('保存失败!');
                    }
                },
                error: function () {
                    layer.msg('保存失败!');
                }
            })
        }
    }

</script>
</body>
</html>