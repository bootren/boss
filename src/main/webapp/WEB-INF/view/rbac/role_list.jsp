<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>首页</title>
    <link href="${pageContext.request.contextPath}/res/lib/jquery.bootgrid-1.2.0/jquery.bootgrid.css" rel="stylesheet">
</head>
<body>
<nav class="breadcrumb">
    <i class="iconfont">󰄫</i> <a class="maincolor" href="${pageContext.request.contextPath}/">首页</a>
    <span class="c-999 en">&gt;</span><span class="c-666">用户角色权限</span>
    <span class="c-999 en">&gt;</span><span class="c-666">角色管理</span>
    <a class="btn btn-success radius r mr-20" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新"><i
            class="icon-refresh"></i></a>
</nav>
<div class="Hui-article">
    <article class="cl pl-20 pr-20 pt-10 pb-10">
        <div class="text-l pt-10">
            <a href="${pageContext.request.contextPath}/rbac/role/edit" class="btn btn-primary radius"><i class="icon-plus"></i> 添加角色</a></span>
        </div>
        <table id="grid-data" class="table table-border table-striped table-hover mt-10">
            <thead>
            <tr>
                <th data-column-id="code">代码</th>
                <th data-column-id="name">名称</th>
                <th data-column-id="value">权限value</th>
                <th data-column-id="commands" data-formatter="commands">操作</th>
            </tr>
            </thead>
        </table>
    </article>
</div>

<!-- Scripts -->
<script src="${pageContext.request.contextPath}/res/lib/jquery.bootgrid-1.2.0/jquery.bootgrid.js"></script>
<script>
    function searchGrid() {
        $("#grid-data").bootgrid("search", Math.random());
    }
    $(function () {
        //查询
        $("#searchBtn").click(function () {
            searchGrid();
        });
        //列表
        var grid = $("#grid-data").bootgrid({
            /***固定配置，一般不需要变化****/
            navigation: 2,
//            rowCount:20,//分页size
            ajax: true,//ajax访问方式
            sorting: false,//禁用列排序
            //转换传递参数以对应后端pageable对象（固定）
            requestHandler: function (request) {
                return pageUtil.requestHandler(request);
            },
            //转换结果以适应数据表格的输出（固定）
            responseHandler: function (response) {
                return pageUtil.responseHandler(response);
            },
            /***以下需要配置****/
            url: "${pageContext.request.contextPath}/rbac/role/gridData",
            post: function () {//封装传递参数
                return {};
            },
            formatters: {
                "universityName": function (column, row) {
                    return !row.universityName ? "公共平台" : row.universityName;
                },
                "published": function (column, row) {
                    return row.published == true ? "已发布" : "草稿";
                },
                "commands": function (column, row) {
                    var t = "";
                    if (row.code != 'system') {
                        t += "<button title=\"资源\" type=\"button\" class=\"btn btn-xs btn-default command-resource\" data-row-no=\"" + row.code + "\"><span class=\"icon icon-cogs\"></span></button> ";
                        t += "<button title=\"编辑\" type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-row-no=\"" + row.code + "\"><span class=\"icon icon-edit\"></span></button> ";
                        if (row.type != "system") {
                            t += "<button title=\"删除\" type=\"button\" class=\"btn btn-xs btn-default command-delete\" data-row-no=\"" + row.code + "\"><span class=\"icon icon-trash\"></span></button>";
                        }
                    }
                    return t;
                }
            }
        }).on("loaded.rs.jquery.bootgrid", function () {
            //编写相关事件
            grid.find(".command-view").on("click", function (e) {
                <%--window.location = '${pageContext.request.contextPath}/notice/view/' + $(this).data("row-no");--%>
            }).end().find(".command-edit").on("click", function (e) {
                window.location = '${pageContext.request.contextPath}/rbac/role/edit?code=' + $(this).data("row-no");
            }).end().find(".command-resource").on("click", function (e) {
                window.location = '${pageContext.request.contextPath}/rbac/role/resource?code=' + $(this).data("row-no");
            }).end().find(".command-delete").on("click", function (e) {
                var code = $(this).data("row-no");
                layer.confirm('确定要删除吗？', {
                    btn: ['确定', '取消'], //按钮
                    shade: false //不显示遮罩
                }, function () {
                    $.post('${pageContext.request.contextPath}/rbac/role/remove/' + code, function (data) {
                        if (data && data.ok == true) {
                            layer.msg("删除成功", {icon: 6});
                            searchGrid();
                        }
                    });
                }, function () {
                });

            });
        });
    });
</script>
</body>
</html>