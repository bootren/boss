<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>首页</title>
    <link href="${pageContext.request.contextPath}/res/lib/zTree_v3/css/metroStyle/metroStyle.css" rel="stylesheet">
</head>
<body>
<nav class="breadcrumb">
    <i class="iconfont">󰄫</i> <a class="maincolor" href="${pageContext.request.contextPath}/">首页</a>
    <span class="c-999 en">&gt;</span><span class="c-666">角色资源权限</span>
    <span class="c-999 en">&gt;</span><span class="c-666">角色资源授权</span>
    <a class="btn btn-success radius r mr-20" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新">
        <i class="icon-refresh"></i>
    </a>
</nav>
<div class="Hui-article">
    <article class="cl pl-20 pr-20 pt-10 pb-10">

        <div id="resource-div" class="col-12">
            <div id="resource-tree-div" class="col-4">
                <h4>资源选择 <input class="btn btn-primary radius" type="button" onclick="formSubmit();" value="&nbsp;&nbsp;提交&nbsp;&nbsp;"></h4>

                <div id="tree" class="ztree"></div>
            </div>
        </div>
    </article>
</div>
<style>

</style>
<!-- Scripts -->
<script src="${pageContext.request.contextPath}/res/lib/zTree_v3/js/jquery.ztree.all-3.5.min.js"></script>
<script>

    <!--
    var setting = {
//        view: {
//            selectedMulti: false
//        },
        <c:if test="${param.code ne 'system'}">
        check: {
            enable: true
        },
        </c:if>
        data: {
            simpleData: {
                enable: true,
                idKey: "code",
                pIdKey: "parentCode"
            }
        },
        callback: {
            onClick: function (event, treeId, treeNode) {

            }
        }
    };
    //-->

    var zNodes = [];

    $(function () {
        init();
    });

    function init() {
        $.ajax({
            url: "${pageContext.request.contextPath}/rbac/resource/treeData",
            type:"post",
            async: false,
            cache: false,
            success: function (data) {
                zNodes = data || [];
                $.fn.zTree.init($("#tree"), setting, zNodes);
                initData();
            }
        });
    }

    function initData() {
        // 获取当前所有的资源code数组  可以从model里获取  或 通过ajax获取
        var arr = [];
        $.ajax({
            url: "${pageContext.request.contextPath}/rbac/role/resourceCodeList?roleCode=${param.code}",method:"post", async: false, cache: false, success: function (data) {
                arr = data || [];
                setResourcesTree(arr);
            }
        });

//        setResourcesTree(arr);
    }

    /**
     * 角色对应的资源树赋值
     * @param arr
     */
    function setResourcesTree(arr) {
        var treeObj = $.fn.zTree.getZTreeObj("tree");
        $(arr).each(function (i, resourceCode) {
            //获取节点
            var node = treeObj.getNodeByParam("code", resourceCode, null);
            treeObj.checkNode(node, true, false);
        });
        treeObj.expandAll(true);
    }
    /**
     * 资源树选中状态清空
     */
    function clearResourceTree() {
        var treeObj = $.fn.zTree.getZTreeObj("tree");
        var nodes = treeObj.getCheckedNodes(true);
        for (var i = 0, l = nodes.length; i < l; i++) {
            treeObj.checkNode(nodes[i], false, true);
        }
    }

    /**
     *  为角色获取选中的资源对象
     */
    function getCheckedResources() {
        var roleResourceCodes = [];
        var treeObj = $.fn.zTree.getZTreeObj("tree");
        var nodes = treeObj.getCheckedNodes(true);
        for (var i = 0, l = nodes.length; i < l; i++) {
            roleResourceCodes.push(nodes[i].code);
        }
        return roleResourceCodes;
    }

    function formSubmit() {
        var res = getCheckedResources();
        console.log(res)
        if (res.length == 0) {
            layer.msg("必须选择资源！", {icon: 5});
            return;
        }
        $.ajax({
            async: false,
            data: {roleCode: "${param.code}", res: res},
            url: "${pageContext.request.contextPath}/rbac/role/grant",
            type: "post",
            cache: false,
            success: function (data) {
                if (data.ok == true) {
                    layer.msg("授权成功！", {icon: 6});
                    window.location="${pageContext.request.contextPath}/rbac/role/list";
                } else {
                    layer.msg("授权失败！", {icon: 5});
                }

            }
        });
    }
</script>
</body>
</html>