<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>首页</title>
    <link href="${pageContext.request.contextPath}/res/lib/zTree_v3/css/metroStyle/metroStyle.css" rel="stylesheet">
</head>
<body>
<nav class="breadcrumb">
    <i class="iconfont">󰄫</i> <a class="maincolor" href="${pageContext.request.contextPath}/">首页</a>
    <span class="c-999 en">&gt;</span><span class="c-666">角色资源权限</span>
    <span class="c-999 en">&gt;</span><span class="c-666">资源管理</span>
    <a class="btn btn-success radius r mr-20" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新">
        <i class="icon-refresh"></i>
    </a>
</nav>
<div class="Hui-article">
    <article class="cl pl-20 pr-20 pt-10 pb-10">
        <div id="resource-div" class="col-12">
            <div id="resource-tree-div" class="col-4">
                <h4>资源</h4>

                <div id="tree" class="ztree"></div>
            </div>
            <div id="resource-form-div" class="col-8 pl-20">
                <h4>资源属性配置</h4>

                <form id="resource-form" class="form form-horizontal">
                    <input type="hidden" name="id" id="id" value=""/>
                    <input type="hidden" name="tId" id="tId" value=""/>

                    <div class="row cl">
                        <label class="form-label col-2"><span class="c-red">*</span>code：</label>

                        <div class="formControls col-5">
                            <input type="text" class="input-text" data-validation-engine="validate[required,maxSize[60]]" value="" placeholder="" id="code" name="code"/>
                        </div>
                        <div class="col-5"></div>
                    </div>
                    <div class="row cl">
                        <label class="form-label col-2"><span class="c-red">*</span>name：</label>

                        <div class="formControls col-5">
                            <input type="text" class="input-text" data-validation-engine="validate[required,maxSize[60]]" value="" placeholder="" id="name" name="name"/>
                        </div>
                        <div class="col-5"></div>
                    </div>
                    <div class="row cl">
                        <label class="form-label col-2">url：</label>

                        <div class="formControls col-5">
                            <input type="text" class="input-text disabled" value="" placeholder="" id="url" name="url" readonly="readonly"/>
                        </div>
                        <div class="col-5"></div>
                    </div>
                    <div class="row cl">
                        <label class="form-label col-2"><span class="c-red">*</span>parentCode：</label>

                        <div class="formControls col-5">
                            <input type="text" class="input-text disabled" readonly="readonly" data-validation-engine="validate[required,maxSize[60]]" value="" placeholder=""
                                   id="parentCode" name="parentCode"/>
                        </div>
                        <div class="col-5"></div>
                    </div>
                    <div class="row cl">
                        <label class="form-label col-2"><span class="c-red">*</span>appCode：</label>

                        <div class="formControls col-5">
                            <input type="text" class="input-text disabled" data-validation-engine="validate[required,maxSize[60]]" value="" placeholder=""
                                   id="appCode" name="appCode"/>
                        </div>
                        <div class="col-5"></div>
                    </div>
                    <div class="row cl">
                        <label class="form-label col-2"><span class="c-red">*</span>类型：</label>

                        <div class="formControls col-5">
                            <select class="select" size="1" name="type" id="type" data-validation-engine="validate[required]">
                                <option value="root">资源ROOT</option>
                                <option value="application">应用</option>
                                <option value="menu">菜单</option>
                                <option value="url">url</option>
                            </select>
                        </div>
                        <div class="col-5"></div>
                    </div>

                    <div class="row cl">
                        <label class="form-label col-2">value：</label>

                        <div class="formControls col-5">
                            <input type="text" class="input-text" data-validation-engine="validate[maxSize[60]]" value="" placeholder="" id="value" name="value"/>
                        </div>
                        <div class="col-5"></div>
                    </div>

                    <div class="row cl">
                        <div class="col-7 col-offset-4">
                            <input class="btn btn-primary radius" type="button" onclick="formSubmit();" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
                            <input class="btn radius" type="button" onclick="reFillForm();" value="&nbsp;&nbsp;重置&nbsp;&nbsp;">
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </article>
</div>
<style>
    #resource-div {
        background: #fff;
        padding: 10px;;
    }

    #resource-tree-div {
        min-height: 600px;
        border-right: 1px #ddd solid;
    }

</style>
<!-- Scripts -->
<script src="${pageContext.request.contextPath}/res/lib/zTree_v3/js/jquery.ztree.all-3.5.min.js"></script>
<script>

    <!--
    var setting = {
        view: {
            addHoverDom: addHoverDom,
            removeHoverDom: removeHoverDom,
            selectedMulti: false
        },
        edit: {
            enable: true,
            showRemoveBtn: false,
            showRenameBtn: false,
            drag: {
                isCopy: false,
                isMove: false
            }
        },
        data: {
            simpleData: {
                enable: true,
                idKey: "code",
                pIdKey: "parentCode"
            }
        },
        callback: {
            onClick: function (event, treeId, treeNode) {
                if (treeNode.url != "") {
                    treeNode.href = 'javascript:void(0)';
                }
                setResourceForm(treeNode, false);
            }
        }
    };

    var newCount = 1;
    function addHoverDom(treeId, treeNode) {
        var sObj = $("#" + treeNode.tId + "_span");
        if (treeNode.editNameFlag || $("#addBtn_" + treeNode.tId).length > 0 ||  $("#removeBtn_" + treeNode.tId).length > 0) return;
        if (treeNode.type != "root") {
            var deleteStr = "<span class='button remove' id='removeBtn_" + treeNode.tId + "' title='删除' onfocus='this.blur();'></span>";
            sObj.after(deleteStr)
            var btn1 = $("#removeBtn_" + treeNode.tId);
            if (btn1) btn1.bind("click", function () {
                deleteNode(treeNode);
                return false;
            });
        }
        if (treeNode.type != "url") {
            var addStr = "<span class='button add' id='addBtn_" + treeNode.tId + "' title='添加' onfocus='this.blur();'></span>";
            sObj.after(addStr);
            var btn = $("#addBtn_" + treeNode.tId);
            if (btn) btn.bind("click", function () {
                addNode(treeNode);
                return false;
            });
        }
    }
    function deleteNode(treeNode) {
        var flag = false, msg;
        if (treeNode.children !=null&&treeNode.children.length>0) {
            layer.msg("有子节点不允许删除!", {icon: 5});
            return;
        }
        var zTree = $.fn.zTree.getZTreeObj("tree");
        if (treeNode.code == undefined) {
            zTree.removeNode(treeNode);
            layer.msg("删除成功!", {icon: 6});
            return;
        }
        if (confirm('确定要删除吗？')) {
            try {
                $.ajax({
                    url: '${pageContext.request.contextPath}/rbac/resource/remove/' + treeNode.code,
                    async: false,
                    type: "post",
                    success: function (data) {
                        flag = data.ok;
                        msg = data.message || "";
                        resetResourceForm();
                        zTree.removeNode(treeNode);
                    }
                });
            } catch (e) {
            }
            layer.msg("删除" + (flag == true ? "成功!" : "失败!" + msg), {icon: (flag == true ? 6 : 5)});
            return flag;
        }
        return false;
    }
    function addNode(treeNode) {
        var zTree = $.fn.zTree.getZTreeObj("tree");
        var node = zTree.addNodes(treeNode, {parentCode: treeNode.code, name: "新建资源",children:null});
        zTree.selectNode(node[0]);

        setResourceForm({parentCode: treeNode.code, name: "新建资源", tId: node[0].tId, type: treeNode.type,children:null}, true)
    }

    function removeHoverDom(treeId, treeNode) {
        $("#addBtn_" + treeNode.tId).unbind().remove();
        $("#removeBtn_" + treeNode.tId).unbind().remove();
    }
    //-->

    //资源类型
    var typeResource = ["application", "menu", "url"];
    var zNodes = [];

    function arrayFilter(arrObj, objProperty, objValue) {
        return $.grep(arrObj, function (cur, i) {
            return cur[objProperty] != objValue;
        });
    }

    $(function () {
        $('#resource-form').validationEngine({promptPosition: "centerRight"});
        $('.skin-minimal input').iCheck({
            checkboxClass: 'icheckbox-blue',
            radioClass: 'iradio-blue',
            increaseArea: '20%'
        });
        $.ajax({
            url: "${pageContext.request.contextPath}/rbac/resource/treeData",
            async: false,
            cache: false,
            success: function (data) {
                zNodes = data || [];
                var zTree = $.fn.zTree.init($("#tree"), setting, zNodes);
                //展开一级节点
                var nodes = zTree.getNodesByFilter(function (node) {
                    return (node.level < 1);
                });
                $(nodes).each(function (i, item) {
                    zTree.expandNode(item);
                });
            }
        })

        $("#addBtn").click(function () {
            addNode();
        });


    });

    var optionRoot = $('<option value="root">资源ROOT</option>');
    var optionApplication = $('<option value="application">应用</option>');
    var optionMenu = $('<option value="menu">菜单</option>');
    var optionUrl = $('<option value="url">url</option>');
    /**
     *展示资源form
     * @param role
     */
    function setResourceForm(data, isNew) {
        resetResourceForm();
        var form = $("#resource-form");
        var sel = $("#type"), url = $("#url");

        switch (data.type) {
            //根节点只能显示 基于application -- 菜单 url    （菜单--ul） （应用-菜单   菜单-菜单）
            case "root":
                if (!isNew) {
                    sel.append(optionRoot.clone().prop("selected", true));
                } else {
                    sel.append(optionApplication.clone());
                }
                url.addClass("disabled").prop("readonly", true);
                break;
            case "application":
                if (!isNew) {
                    sel.append(optionApplication.clone().prop("selected", true));
                } else {
                    sel.append(optionMenu.clone());
                    sel.append(optionUrl.clone());
                }
                url.removeClass("disabled").prop("readonly", false);
                break;
            case "menu":
                sel.append(optionMenu.clone());
                sel.append(optionUrl.clone());
                url.addClass("disabled").prop("readonly", true);
                break;
            case "url":
                if (!isNew) {
                    sel.append(optionUrl.clone().prop("selected", true));
                }
                url.addClass("disabled").prop("readonly", true);
                break;
            default :


        }

        $("#id", form).val(data.id);
        $("#code", form).val(data.code).addClass((data.id != undefined && data.id != '') || data.type == "root" ? 'disabled' : '');
        $("#name", form).val(data.name);
        $("#value", form).val(data.value);
        $("#tId", form).val(data.tId);
        $("#url", form).val(data.url || "");
        $("#appCode", form).val(data.appCode);
        $("#parentCode", form).val(data.parentCode);
        $("select#type option[value='" + data.type + "']").prop("selected", true);

    }
    function reFillForm() {
        var node = $.fn.zTree.getZTreeObj("tree").getNodeByTId($("#tId").val());
        setResourceForm(node, false);
    }
    /**
     *资源form重置
     */
    function resetResourceForm() {
        var form = $("#resource-form");
        $("input:not(:button)", form).val('').removeClass("disabled");
//        $("#code").removeClass("disabled");
        $("select#type option").remove();
    }


    function formSubmit() {
        if ($('#resource-form').validationEngine("validate")) {
            var tId = $("#tId").val();
            $.ajax({
                url: '${pageContext.request.contextPath}/rbac/resource/save',
                type: 'post',
                data: $("#resource-form").serialize(),
                async: false,
                cache: false,
                success: function (data) {
                    if (data.ok == true) {
                        layer.msg('保存成功!');
                        var zTree = $.fn.zTree.getZTreeObj("tree");
//                        console.log(data.object);
//                        data.object.tId = tId;
                        var node = $.extend(true, zTree.getNodeByTId(tId), data.object);
                        console.log(node);
                        zTree.updateNode(node);
                        resetResourceForm();
                    } else {
                        layer.msg('保存失败!');
                    }
                },
                error: function () {
                    layer.msg('保存失败!');
                }
            })
        }
    }
</script>
</body>
</html>