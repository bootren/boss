<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>首页</title>
    <script src="${pageContext.request.contextPath}/res/js/area.js"></script>
    <script src="${pageContext.request.contextPath}/res/js/industry.js"></script>
    <script src="${pageContext.request.contextPath}/res/js/positions.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/res/css/xsr-component.css" type="text/css"/>
</head>
<body>
<nav class="breadcrumb">
    <i class="iconfont">󰄫</i> <a class="" href="/">首页</a>
    <span class="c-gray en">&gt;</span> 自定组件
</nav>
<div class="Hui-article">
    <article class="cl pl-20 pr-20 pt-10 pb-10">
        <form action="" method="post" class="form form-horizontal" id="form-notice-add">

            <div class="row cl">
                <label class="form-label col-2">选择地区：</label>

                <div class="formControls col-5">
                    <input type="text" class="input-text" readonly="readonly" placeholder="选择地区" id="area" name="area">
                </div>
                <div class="col-5">
                    <input type="button" value="获取选择结果" id="testBtn"/>
                </div>
            </div>

            <div class="row cl">
                <label class="form-label col-2">选择行业：</label>

                <div class="formControls col-5">
                    <input type="text" class="input-text" readonly="readonly" placeholder="选择行业" id="industry" name="industry">
                </div>
                <div class="col-5">
                    <input type="button" value="获取选择结果" id="testBtn1"/>
                </div>
            </div>

            <div class="row cl">
                <label class="form-label col-2">选择职位：</label>

                <div class="formControls col-5">
                    <input type="text" class="input-text" readonly="readonly" placeholder="选择职位" id="position" name="position">
                </div>
                <div class="col-5">
                    <input type="button" value="获取选择结果" id="testBtn2"/>
                </div>
            </div>
        </form>
    </article>
</div>

<!-- Scripts -->
<script>
    $(function () {
        $("#area").area();
        $("#testBtn").click(function () {
            var data = $("#area").area("getData");
            console.log(data);
        })

        $("#industry").industry();
        $("#testBtn1").click(function () {
            var data = $("#industry").industry("getData");
            console.log(data);
        })

        $("#position").position();
        $("#testBtn2").click(function () {
            var data = $("#position").position("getData");
            console.log(data);
        })


    });


</script>
</body>
</html>