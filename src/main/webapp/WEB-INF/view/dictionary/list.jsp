<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>首页</title>
</head>
<body>
<nav class="breadcrumb">
    <i class="iconfont">󰄫</i> <a class="maincolor" href="${pageContext.request.contextPath}/">首页</a>
    <span class="c-999 en">&gt;</span><span class="c-666">数据仓库</span>
    <span class="c-999 en">&gt;</span><span class="c-666">数据字典</span>
    <a class="btn btn-success radius r mr-20" style="line-height:1.6em;margin-top:3px"
       href="javascript:location.replace(location.href);" title="刷新"><i
            class="icon-refresh"></i></a>
</nav>
<div class="Hui-article">
    <article class="cl pl-20 pr-20 pt-10 pb-10">
        <div class="col-12">
            <form action="" method="post" class="form form-horizontal" id="form-dic">
                <input type="hidden" name="id" value="" id="id"/>

                <div class="row cl">
                    <label class="form-label col-2"><span class="c-red">*</span>code：</label>

                    <div class="formControls col-5">
                        <input type="text" class="input-text" data-validation-engine="validate[required,maxSize[30]]"
                               value="" placeholder="请输入code" id="code" name="code">
                    </div>
                    <div class="col-5"></div>
                </div>

                <div class="row cl">
                    <label class="form-label col-2"><span class="c-red">*</span>字典名称：</label>

                    <div class="formControls col-5">
                        <input type="text" class="input-text" data-validation-engine="validate[required,maxSize[30]]"
                               value="" placeholder="请输入字典名称" id="name" name="name">
                    </div>
                    <div class="col-5"></div>
                </div>

                <div class="row cl">
                    <label class="form-label col-2"><span class="c-red">*</span>字典值：</label>

                    <div class="formControls col-5">
                        <input type="text" class="input-text" data-validation-engine="validate[required,maxSize[30]]"
                               value="" placeholder="请输入字典值" id="value" name="value">
                    </div>
                    <div class="col-5"></div>
                </div>
                <div class="row cl">
                    <div class="col-7 col-offset-4">
                        <input class="btn btn-primary radius" type="button" onclick="formSubmit();"
                               value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
                    </div>
                </div>
            </form>
        </div>
        <div id="dicAll" class="col-12">

        </div>
        <ul id="tpl" class="dicGroup" style="display: none;"></ul>
    </article>
</div>
<style>
    .dicGroup {
        width: 300px;
        background-color: #fff;
        padding: 20px;
        line-height: 30px;
        display: inline-block;
        margin: 10px;
        vertical-align: top;
        min-height: 150px;
    }

    .dicGroup li {
        border-bottom: 1px dotted #ddd;
    }

    .dicGroup a {
        margin: 0 5px;
        float: right;
    }
</style>
<!-- Scripts -->
<script>
    function resetForm() {
        $("#form-dic #id").val("");
        $("#form-dic #code").val("");
        $("#form-dic #name").val("");
        $("#form-dic #value").val("");
    }
    function formSubmit() {
        if ($('#form-industry-add').validationEngine("validate")) {
            $.ajax({
                async: false,
                data: $("#form-dic").serialize(),
                url: "${pageContext.request.contextPath}/dic/dictionary/save",
                type: "post",
                cache: false,
                success: function (data) {
                    if (data.ok == true) {
                        layer.msg("保存成功！", {icon: 6});
                        resetForm();
                        setDicRow(data.object);
                        setData(data.object);
                    } else {
                        layer.msg("保存失败！", {icon: 5});
                    }

                }
            });
        }
    }
    $(function () {

        $('#form-dic').validationEngine({promptPosition: "centerRight"});
        $.ajax({
            url: "${pageContext.request.contextPath}/dic/dictionary/treeData",
            type: "post",
            async: false,
            cache: false,
            success: function (data) {
                if (data && data.length > 0) {
                    $(data).each(function (i, item) {
                        resetForm();
                        setDicRow(item);
                        $.post("${pageContext.request.contextPath}/dic/dictionary/dicListByCode/" + item.code, function (data) {
                            if (data && data.length > 0) {
                                $(data).each(function (i, item) {
                                    setData(item);
                                });
                            }
                        });
                    });
                }
            }
        })


        $("#dicAll").delegate(".item-edit", "click", function () {
            var value = $(this).parent().attr("data-value");
            var id = $(this).parent().attr("data-id");
            var code = $(this).closest(".dicGroup").attr("data-code");
            var name = $(this).closest(".dicGroup").attr("data-name");
            $("#form-dic #id").val(id);
            $("#form-dic #code").val(code);
            $("#form-dic #name").val(name);
            $("#form-dic #value").val(value);
        }).delegate(".item-delete", "click", function () {
            var li = $(this).closest("li");
            var id = li.attr("data-id");
            layer.confirm('确定要删除吗？', {
                btn: ['确定', '取消'], //按钮
                shade: false //不显示遮罩
            }, function () {
                $.post("${pageContext.request.contextPath}/dic/dictionary/remove/" + id, function (data) {
                    layer.msg("删除成功！", {icon: 6});
                    li.remove();
                });
            }, function () {
            });
        }).delegate(".item-add", "click", function () {
            var code = $(this).parent().attr("data-code");
            var name = $(this).parent().attr("data-name");
            resetForm();
            $("#form-dic #code").val(code);
            $("#form-dic #name").val(name);
        });
    });

    function setDicRow(data) {
        if (data.code == undefined || data.code == "") {
            layer.msg("必须有code", {icon: 5});
            return false;
        }
        if ($("#dic_row_" + data.code).length == 0) {
            console.log("创建行");
            var row = $("#tpl").clone(true).css("display", "inline-block").attr("id", "dic_row_" + data.code).html('<strong>' + data.name + '<em style="color:Red;">' + data.code + '</em>' + '</strong>')
                    .attr("data-code", data.code).attr("data-name", data.name)
                    .append('<a href="javascript:void(0);" class="item-add"><i class="icon icon-plus"></i></a>');
            $("#dicAll").append(row);
        }
    }

    function setData(data) {
        var code = data.code;
        var row = $("#dic_row_" + code);
        if (row.length == 0) {
            layer.msg("没有找到" + code + "行", {icon: 5});
            return;
        }
        if (row.find("#dic_item_" + data.id).length > 0) {
            row.find("#dic_item_" + data.id).remove();
        }
        row.append('<li id="dic_item_' + data.id + '" data-value="' + data.value + '" data-id="' + data.id + '">' + data.value
                + '<a href="javascript:void(0)" class="item-edit"><i class="icon icon-edit"></i></a><a href="javascript:void(0)" class="item-delete"><i class="icon icon-remove"></i></a></li>')
    }
</script>
</body>
</html>