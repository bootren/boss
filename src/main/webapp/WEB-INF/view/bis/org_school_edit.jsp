<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>首页</title>
    <script src="http://p1.tongxuepie.com/boss/area/areadata.js"></script>
    <script src="http://f1.tongxuepie.com/boss/category/zh_CN1443000844302.js"></script>
    <script src="${pageContext.request.contextPath}/res/js/area.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/res/css/xsr-component.css" type="text/css"/>
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=kKhFO1xe9ktEWqiLuhxGSWGo"></script>
</head>
<body>
<nav class="breadcrumb">
    <i class="iconfont">󰄫</i> <a class="" href="${pageContext.request.contextPath}/">首页</a>
    <span class="c-gray en">&gt;</span> <a href="${pageContext.request.contextPath}/rbac/org/list">组织管理</a>
    <span class="c-gray en">&gt;</span> <c:if test="${empty model}">新增</c:if><c:if test="${not empty model}">编辑</c:if>组织
    <a class="btn btn-success radius r mr-20"
       style="line-height:1.6em;margin-top:3px" href="${pageContext.request.contextPath}/rbac/org/list" title="返回">
        <i class="icon-reply"></i>
    </a>
</nav>
<div class="Hui-article">
    <article class="cl pl-20 pr-20 pt-10 pb-10">
        <form action="" method="post" class="form form-horizontal" id="form-add">
            <input type="hidden" name="id" value="${model.id}"/>

            <div class="row cl">
                <label class="form-label col-2">编号：</label>

                <div class="formControls col-5">
                    <c:choose>
                        <c:when test="${not empty model}">
                            <input type="text" class="input-text disabled" value="${model.code}" readonly="readonly" id="code" name="code">
                        </c:when>
                        <c:otherwise>
                            <input type="text" class="input-text" id="code" value="${model.code}" data-validation-engine="validate[required,maxSize[128]]" placeholder="填写机构代码，高校填写高校代码" name="code">
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="col-5"></div>
            </div>
            <div class="row cl">
                <label class="form-label col-2"><span class="c-red">*</span>名称：</label>

                <div class="formControls col-5">
                    <input type="text" class="input-text" data-validation-engine="validate[required,maxSize[255]]" value="${model.name}" placeholder="填写机构名称" id="name" name="name">
                </div>
                <div class="col-5"></div>
            </div>
            <div class="row cl">
                <label class="form-label col-2"><span class="c-red">*</span>类型：</label>

                <div class="formControls col-5">
                    <input type="text" class="input-text disabled" data-validation-engine="validate[required,maxSize[255]]" value="school" placeholder="" id="type" name="type">
                </div>
                <div class="col-5"></div>
            </div>
            <div class="row cl">
                <label class="form-label col-2"><span class="c-red">*</span>联系人：</label>

                <div class="formControls col-5">
                    <input type="text" class="input-text " data-validation-engine="validate[required,maxSize[255]]" value="${model.contact}" placeholder="填写联系人" id="contact"
                           name="contact">
                </div>
                <div class="col-5"></div>
            </div>
            <div class="row cl">
                <label class="form-label col-2"><span class="c-red">*</span>手机号码：</label>

                <div class="formControls col-5">
                    <input type="text" class="input-text " data-validation-engine="validate[required,maxSize[255]]" value="${model.mobile}" placeholder="填写手机号码" id="mobile"
                           name="mobile">
                </div>
                <div class="col-5"></div>
            </div>
            <div class="row cl">
                <label class="form-label col-2"><span class="c-red">*</span>电子邮箱：</label>

                <div class="formControls col-5">
                    <input type="text" class="input-text " data-validation-engine="validate[required,maxSize[255]]" value="${model.email}" placeholder="填写电子邮箱" id="email" name="email">
                </div>
                <div class="col-5"></div>
            </div>
            <div class="row cl">
                <label class="form-label col-2"><span class="c-red">*</span>联系电话：</label>

                <div class="formControls col-5">
                    <input type="text" class="input-text " data-validation-engine="validate[required,maxSize[255]]" value="${model.phone}" placeholder="填写联系电话" id="phone" name="phone">
                </div>
                <div class="col-5"></div>
            </div>
            <div class="row cl">
                <label class="form-label col-2"><span class="c-red">*</span>传真：</label>

                <div class="formControls col-5">
                    <input type="text" class="input-text " data-validation-engine="validate[required,maxSize[255]]" value="${model.fax}" placeholder="填写传真" id="fax" name="fax">
                </div>
                <div class="col-5"></div>
            </div>

            <div class="row cl">
                <label class="form-label col-2">省市：</label>

                <div class="formControls col-2">

                    <input type="text" class="input-text" data-validation-engine="validate[required,maxSize[255]]" value="${model.province}" placeholder="选择省份" readonly="readonly" id="province"
                           name="province" readonly="readonly"/>
                </div>
                <div class="formControls col-2">
                    <input type="text" class="input-text" data-validation-engine="validate[required,maxSize[255]]" value="${model.city}" placeholder="选择城市" id="city" name="city" readonly="readonly">
                </div>
                <div class="col-4">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-2"><span class="c-red">*</span>详细地址：</label>

                <div class="formControls col-5">
                    <input type="text" class="input-text " data-validation-engine="validate[required,maxSize[255]]" value="${model.address}" placeholder="填写详细地址" id="address"
                           name="address">
                </div>
                <div class="col-5"></div>
            </div>

            <div class="row cl">
                <label class="form-label col-2">地图选点：</label>

                <div class="col-5">
                    <div id="map" style="width: 100%; height: 380px;"></div>
                </div>
                <div class="col-5"></div>
            </div>
            <div class="row cl">
                <label class="form-label col-2"></label>

                <div class="formControls col-2">

                    <input type="text" class="input-text " data-validation-engine="validate[required,maxSize[255]]" value="${model.latitude}" placeholder="纬度" id="latitude"
                           name="latitude" readonly="readonly">
                </div>
                <div class="formControls col-2">

                    <input type="text" class="input-text " data-validation-engine="validate[required,maxSize[255]]" value="${model.longitude}" placeholder="经度" id="longitude"
                           name="longitude" readonly="readonly">
                </div>
                <div class="col-6"></div>
            </div>

            <div class="row cl">
                <div class="col-7 col-offset-4">
                    <input class="btn btn-primary radius" type="button" onclick="formSubmit();" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
                    <input class="btn radius" type="button" onclick="location='${pageContext.request.contextPath}/rbac/org/list'" value="&nbsp;&nbsp;返回&nbsp;&nbsp;">
                </div>
            </div>
        </form>
    </article>
</div>

<script>
    $(function () {
        $("#province,#city").area({
            inputWidth: 20,
            replaceEl: false,
            callback: function (data) {
                $("#province").val(data[0].province);
                $("#city").val(data[0].city);
                if ($("#address").val().indexOf(data[0].county) != 0) {
                    $("#address").val(data[0].county);
                    changes();
                }
            }
        });


        $('#form-add').validationEngine({promptPosition: "centerRight"});

//        $('.skin-minimal input').iCheck({
//            checkboxClass: 'icheckbox-blue',
//            radioClass: 'iradio-blue',
//            increaseArea: '20%'
//        });

        $("#address").change(function () {
            changes();
        }).keyup(function () {
            changes();
        });
    });

    function formSubmit() {
        if ($('#form-add').validationEngine("validate")) {
            $.ajax({
                url: '${pageContext.request.contextPath}/rbac/org/save',
                type: 'post',
                data: $("#form-add").serialize(),
                async: false,
                cache: false,
                success: function (data) {
                    if (data.ok == true) {
                        layer.msg('保存成功!');
                        window.location = '${pageContext.request.contextPath}/rbac/org/list';
                    } else {
                        layer.msg(data.message);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var errorObj = eval('(' + XMLHttpRequest.responseText + ')');
                    console.log(XMLHttpRequest);
                    layer.msg(errorObj.message);
                }
            })
        }
    }


    /********百度地图相关************/
    var top_right_navigation = new BMap.NavigationControl({anchor: BMAP_ANCHOR_TOP_RIGHT, type: BMAP_NAVIGATION_CONTROL_SMALL}); //右上角
    // 百度地图API功能
    var map = new BMap.Map("map");
    map.addControl(top_right_navigation);
    map.enableScrollWheelZoom(true);//允许缩放地图
    //      marker.setAnimation(BMAP_ANIMATION_BOUNCE);
    var geoc = new BMap.Geocoder();
    var marker ;
    if ($("#longitude").val() != "" && $("#latitude").val() != "") {
        var lng = $("#longitude").val(), lat = $("#latitude").val();
        var point = new BMap.Point(lng, lat);
        map.centerAndZoom(point, 16);
        marker = new BMap.Marker(point, 16);// 创建标注
        map.addOverlay(marker);             // 将标注添加到地图中
        marker.enableDragging();          // 可拖拽
        // 创建地址解析器实例
        marker.addEventListener('dragend', function (e) {
            var pt = e.point;
            geoc.getLocation(pt, function (rs) {
                var addComp = rs.addressComponents;
                document.getElementById('longitude').value = pt.lng;
                document.getElementById('latitude').value = pt.lat;
            });
        });
    } else {
        function myFun(result) {
            var cityName = result.name;
            map.centerAndZoom(cityName, 16);   //关于setCenter()可参考API文档---”传送门“
        }

        var myCity = new BMap.LocalCity();
        myCity.get(myFun);
    }

    function changes() {
        var address = $("#province").val() + $("#city").val() + $("#address").val();
        if (address != "") {
            // 将地址解析结果显示在地图上,并调整地图视野
            geoc.getPoint(address, function (point) {
                if (point) {
                    map.centerAndZoom(point, 16);
                    marker = new BMap.Marker(point);
                    map.addOverlay(marker);
                    marker.enableDragging();          // 可拖拽
                    document.getElementById('longitude').value = point.lng;
                    document.getElementById('latitude').value = point.lat;
                }
            });
        }
    }

</script>
</body>
</html>