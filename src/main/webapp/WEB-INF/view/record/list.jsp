
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>首页</title>
    <link href="${pageContext.request.contextPath}/res/lib/jquery.bootgrid-1.2.0/jquery.bootgrid.css" rel="stylesheet">
</head>
<body>
<nav class="breadcrumb">
    <i class="iconfont">󰄫</i> <a class="maincolor" href="/">首页</a>
    <span class="c-999 en">&gt;</span><span class="c-666">全局日志</span>
    <a class="btn btn-success radius r mr-20" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新"><i
            class="icon-refresh"></i></a>
</nav>
<div class="Hui-article">
    <article class="cl pl-20 pr-20 pt-10 pb-10">
        <%--查询条件 如果有需要的话--%>
        <div class="text-c pt-20 pb-20">
            <input type="text" class="input-text" style="width:250px" placeholder="请输入查询关键字" id="searchInput" name="">
            <button type="button" class="btn btn-success" id="searchBtn" name=""><i class="icon-search"></i> 搜索</button>
            <button type="button" class="btn btn-success" id="downloadBtn" name=""><i class="icon-search"></i> 下载</button>
        </div>
        <%--表格--%>
            <div>
                共 <label id="totalNums"></label> 条
            </div>
        <table id="grid-data" class="table table-border table-striped table-hover mt-10">
            <thead>
            <tr>
                <th data-column-id="username">操作人</th>
                <th data-column-id="url">url</th>
                <th data-column-id="function">操作</th>
                <th data-column-id="ip">ip</th>
                <th data-column-id="startTime">时间</th>
                <th data-column-id="time">耗时</th>
                <th data-column-id="success">执行状态</th>
                <th data-column-id="requestParameters">请求参数</th>
                <th data-column-id="errorMsg">错误信息</th>
                <th data-column-id="commands" data-formatter="commands" data-width="45">操作</th>
            </tr>
            </thead>
        </table>
    </article>
</div>
<!-- Scripts -->
<script src="${pageContext.request.contextPath}/res/lib/jquery.bootgrid-1.2.0/jquery.bootgrid.js"></script>
<%--<script src="/res/lib/My97DatePicker/WdatePicker.js"></script>--%>
<script>
    function searchGrid() {
        $("#grid-data").bootgrid("search", Math.random());
    }
    $(function () {
        //查询
        $("#searchBtn").click(function () {
            searchGrid();
        });
        //下载
        $("#downloadBtn").click(function () {
            window.open("${pageContext.request.contextPath}/record/download?searchKey="+$("#searchInput").val());
        });
        //列表
        var grid = $("#grid-data").bootgrid({
            /***固定配置，一般不需要变化****/
            navigation: 2,
            rowCount:15,//分页size
            ajax: true,//ajax访问方式
            sorting: false,//禁用列排序
            //转换传递参数以对应后端pageable对象（固定）
            requestHandler: function (request) {
                return pageUtil.requestHandler(request);
            },
            //转换结果以适应数据表格的输出（固定）
            responseHandler: function (response) {
                $("#totalNums").text(response.totalElements);
                return pageUtil.responseHandler(response);
            },
            /***以下需要配置****/
            url: "${pageContext.request.contextPath}/record/list/gridData",
            post: function () {//封装传递参数
                return {
                    searchKey: $("#searchInput").val()
                };
            },
            formatters: {
                "commands": function (column, row) {
                    console.log(row.toolTip);
                    return "<button title=\"详情\" type=\"button\" class=\"btn btn-xs btn-default command-view\" data-row-no='" + JSON.stringify(row.toolTip) + "'><span class=\"icon icon-edit\"></span></button> ";
                }
            }
        }).on("loaded.rs.jquery.bootgrid", function () {
            //编写相关事件
                grid.find(".command-view").on("click", function (e) {
                    var no = $(this).data("row-no");
                    layer.open({content:no});
                });
        });
    });
</script>
</body>
</html>