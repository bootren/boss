
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>首页</title>
    <link href="${pageContext.request.contextPath}/res/lib/jquery.bootgrid-1.2.0/jquery.bootgrid.css" rel="stylesheet">
</head>
<body>
<nav class="breadcrumb">
    <i class="iconfont">󰄫</i> <a class="maincolor" href="/">首页</a>
    <span class="c-999 en">&gt;</span><span class="c-666">敏感词管理</span>
    <a class="btn btn-success radius r mr-20" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新"><i
            class="icon-refresh"></i></a>
</nav>
<div class="Hui-article">
    <article class="cl pl-20 pr-20 pt-10 pb-10">
        <%--查询条件 如果有需要的话--%>
        <div class="text-c pt-20 pb-20">
            <input type="text" class="input-text" style="width:250px" placeholder="请输入敏感词" id="searchInput" name="">
            <button type="button" class="btn btn-success" id="searchBtn" name=""><i class="icon-search"></i> 搜索敏感词</button>
        </div>
        <div class="text-l pt-10">
            <a href="${pageContext.request.contextPath}/security/sensitive/edit" class="btn btn-primary radius"><i class="icon-plus"></i> 添加敏感词</a></span>
        </div>
        <%--表格--%>
        <table id="grid-data" class="table table-border table-striped table-hover mt-10">
            <thead>
            <tr>
                <th data-column-id="name">敏感词标题</th>
                <th data-column-id="createTime">创建时间</th>
                <th data-column-id="updateTime">最后编辑时间</th>
                <th data-column-id="commands" data-formatter="commands">操作</th>
            </tr>
            </thead>
        </table>
    </article>
</div>
<!-- Scripts -->
<script src="${pageContext.request.contextPath}/res/lib/jquery.bootgrid-1.2.0/jquery.bootgrid.js"></script>
<%--<script src="/res/lib/My97DatePicker/WdatePicker.js"></script>--%>
<script>
    function searchGrid() {
        $("#grid-data").bootgrid("search", Math.random());
    }
    $(function () {
        //查询
        $("#searchBtn").click(function () {
            searchGrid();
        });
        //列表
        var grid = $("#grid-data").bootgrid({
            /***固定配置，一般不需要变化****/
            navigation: 2,
//            rowCount:20,//分页size
            ajax: true,//ajax访问方式
            sorting: false,//禁用列排序
            //转换传递参数以对应后端pageable对象（固定）
            requestHandler: function (request) {
                return pageUtil.requestHandler(request);
            },
            //转换结果以适应数据表格的输出（固定）
            responseHandler: function (response) {
                return pageUtil.responseHandler(response);
            },
            /***以下需要配置****/
            url: "${pageContext.request.contextPath}/security/sensitive/gridData",
            post: function () {//封装传递参数
                return {
                    searchKey: $("#searchInput").val()
                };
            },
            formatters: {
                "commands": function (column, row) {
                    return "<button title=\"编辑\" type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-row-no=\"" + row.id + "\"><span class=\"icon icon-edit\"></span></button> " +
                            "<button title=\"删除\" type=\"button\" class=\"btn btn-xs btn-default command-delete\" data-row-no=\"" + row.id + "\"><span class=\"icon icon-trash\"></span></button>";
                }
            }
        }).on("loaded.rs.jquery.bootgrid", function () {
            //编写相关事件
                grid.find(".command-edit").on("click", function (e) {
                window.location = '${pageContext.request.contextPath}/security/sensitive/edit?id=' + $(this).data("row-no");
            }).end().find(".command-delete").on("click", function (e) {
                var no = $(this).data("row-no");
                layer.confirm('确定要删除吗？', {
                    btn: ['确定', '取消'], //按钮
                    shade: false //不显示遮罩
                }, function () {
                    $.post('${pageContext.request.contextPath}/security/sensitive/remove/' + no, function (data) {
                        if (data && data.ok == true) {
                            layer.msg("删除成功");
                            searchGrid();
                        }
                    });
                }, function () {
                });
            });
        });
    });
</script>
</body>
</html>