<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
    <title>首页</title>
</head>
    <body>
        <nav class="breadcrumb">
            <i class="iconfont">󰄫</i> <a class="" href="${pageContext.request.contextPath}/">首页</a>
            <span class="c-gray en">&gt;</span> <a href="${pageContext.request.contextPath}/security/sensitive/list">敏感词管理</a>
            <a class="btn btn-success radius r mr-20"
                style="line-height:1.6em;margin-top:3px" href="${pageContext.request.contextPath}/security/sensitive/list" title="返回">
                <i class="icon-reply"></i>
            </a>
        </nav>
        <div class="Hui-article">
            <article class="cl pl-20 pr-20 pt-10 pb-10">
                <h1 class="text-c">${model.name}</h1>
                    <p class="text-c"><small>发表于 <fmt:formatDate value="${model.publishedTime}" pattern="yyyy-MM-dd HH:mm"></fmt:formatDate></small></p>
                <div class="line"></div>
                <div class="pd-20">
                    ${model.content}
                </div>
            </article>
        </div>
    </body>
</html>