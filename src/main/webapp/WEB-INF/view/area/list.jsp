<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>首页</title>
    <link href="${pageContext.request.contextPath}/res/lib/zTree_v3/css/metroStyle/metroStyle.css" rel="stylesheet">
</head>
<body>
<nav class="breadcrumb">
    <i class="iconfont">󰄫</i> <a class="maincolor" href="${pageContext.request.contextPath}/">首页</a>
    <span class="c-999 en">&gt;</span><span class="c-666">数据仓库</span>
    <span class="c-999 en">&gt;</span><span class="c-666">区域</span>
    <a class="btn btn-success radius r mr-20" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新"><i
            class="icon-refresh"></i></a>
</nav>
<div class="Hui-article">
    <article class="cl pl-20 pr-20 pt-10 pb-10">

            <%--<div class="text-c pt-20 pb-20">--%>
                <%--&lt;%&ndash;发布日期范围：&ndash;%&gt;--%>
                <%--&lt;%&ndash;<input type="text" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'endDate\')||\'%y-%M-%d\'}'})" readonly id="startDate" class="input-text Wdate" style="width:120px;">&ndash;%&gt;--%>
                <%--&lt;%&ndash;-&ndash;%&gt;--%>
                <%--&lt;%&ndash;<input type="text" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'startDate\')}',maxDate:'%y-%M-%d'})" readonly id="endDate" class="input-text Wdate" style="width:120px;">&ndash;%&gt;--%>
                <%--<input type="text" class="input-text" style="width:250px" placeholder="请输入区域" id="searchInput" name="">--%>
                <%--<button type="button" class="btn btn-success" id="searchBtn" name=""><i class="icon-search"></i> 搜索区域</button>--%>
            <%--</div>--%>
        <div id="tree" class="ztree"></div>

    </article>
</div>

<!-- Scripts -->
<script src="${pageContext.request.contextPath}/res/lib/zTree_v3/js/jquery.ztree.all-3.5.min.js"></script>
<script>

    <!--
    var setting = {
        view: {
                addHoverDom: addHoverDom,
            removeHoverDom: removeHoverDom,
            selectedMulti: false
        },
        check: {
            enable: true
        },
        data: {
            simpleData: {
                enable: true,
                idKey: "id",
                pIdKey: "parentId"
            }
        },
        edit: {
            enable: true
        },
        callback: {
            onRename: function (event, treeId, treeNode, isCancel) {
                var flag = false;
                $.ajax({
                    url: '${pageContext.request.contextPath}/dic/area/save',
                    async: false,
                    type: "post",
                    data: {id: treeNode.id, name: treeNode.name},
                    success: function (data) {
                        flag = data.ok || false;
                    }
                });
                layer.msg("编辑" + (flag == true ? "成功" : "失败") + "!");
            },
            beforeRemove: function (treeId, treeNode) {
                var id = treeNode.id;
                return deleteNode(id);
//                return false;
            }
        }
    };

    var newCount = 1;
    function addHoverDom(treeId, treeNode) {
        var sObj = $("#" + treeNode.tId + "_span");
        if (treeNode.editNameFlag || $("#addBtn_" + treeNode.tId).length > 0) return;
//        var addStr = "<span class='button add' id='addBtn_" + treeNode.tId
//                + "' title='添加分类' onfocus='this.blur();'></span>";
//        sObj.after(addStr);
        var btn = $("#addBtn_" + treeNode.tId);
        if (btn) btn.bind("click", function () {
            addNode(treeNode);
            return false;
        });
    }

    function deleteNode(id) {
        var flag = false;
        if (confirm('确定要删除吗？')) {
            try {
                $.ajax({
                    url: '${pageContext.request.contextPath}/dic/area/remove/' + id,
                    async: false,
                    type: "post",
                    success: function (data) {
                        flag = data.ok;
                    }
                });
            } catch (e) {
            }
            layer.msg("删除" + (flag == true ? "成功" : "失败") + "!");
            return flag;
        }
        return false;
    }
//    function addNode(treeNode) {
//        var zTree = $.fn.zTree.getZTreeObj("tree");
//        var parentId = treeNode ? treeNode.id : null;
//        var name = "新节点";
//        $.ajax({
//            url: '/dic/area/save',
//            async: false,
//            type: "post",
//            data: {
//                parentId: parentId,
//                name: "新节点"
//            },
//            success: function (data) {
//                layer.msg("添加" + (data.ok == true ? "成功" : "失败") + "!");
//                var o = data.object;
//                zTree.addNodes(treeNode, {id: o, pId: parentId, name: name});
//            }
//        });
//    }

    function removeHoverDom(treeId, treeNode) {
        $("#addBtn_" + treeNode.tId).unbind().remove();
    }
    //-->

    $(function () {
        $.ajax({
            url: "${pageContext.request.contextPath}/dic/area/treeData",
            async: false,
            cache: false,
            success: function (data) {
                zNodes = data || [];
                $.fn.zTree.init($("#tree"), setting, zNodes);
            }
        })

        $("#addBtn").click(function () {
            addNode();
        });
    });
</script>
</body>
</html>