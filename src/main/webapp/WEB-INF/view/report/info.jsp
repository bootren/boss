<%@ page import="org.joda.time.DateTime" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>平台全局统计数据</title>
    <script src="${pageContext.request.contextPath}/res/lib/My97DatePicker/WdatePicker.js"></script>
    <script src="${pageContext.request.contextPath}/res/lib/echarts/echarts-all.js"></script>
    <%--<script src="${pageContext.request.contextPath}/res/lib/html2canvas.js"></script>--%>
    <style>
        table {
            table-layout: fixed;
            border-top: 1px #ddd solid;
            border-right: 1px #ddd solid;
        }

        td {
            padding: 10px;
            border-bottom: 1px #ddd solid;
            border-left: 1px #ddd solid;
        }
    </style>
    <script>
        var chartObject = {
            container: "chartContainer",
            id: "",
            height: 300,
            //展示的图表类型
            viewType: ['line', 'bar'],
            title: "",
            subTitle: "",
            legend: [],
            xAxisType: "category",
            xAxisBoundaryGap: false,
            xAxisData: [],
            yAxisType: "value",
            yAxisLabelFormatter: '{value}',
            series: []
        };


        $(function () {
            fetchData();
            $("#searchBtn").click(function () {
                fetchData();
            })

        });

        function fetchData() {
            $("#" + chartObject.container).empty();

            var startDate = $("#startDate").val();
            var endDate = $("#endDate").val();
            if (startDate == '') {
                alert("请选择开始日期");
                return;
            }
            if (endDate == '') {
                alert("请选择结束日期");
                return;
            }
            var searchData = {
                type: $("#type").val(),
                startDate: $("#startDate").val(),
                endDate: $("#endDate").val()
            }
            $.ajax({
                url: '${pageContext.request.contextPath}/report/info/data',
                async: false,
                cache: false,
                type: "post",
                data: searchData,
                success: function (data) {
                    if (data && data.length > 0) {
                        $.each(data, function (i, item) {
                            viewChart($.extend(true, {}, chartObject, item));
                        });
                    }
                }
            });
//            print('平台统计');
        }

        function viewChart(dataOption) {
            var chartView = $('<div id="' + dataOption.id + '" class="chart" style="margin-bottom:30px;margin-top:20px;height:' + dataOption.height + 'px;"></div>');
            $.each(dataOption.series, function (i, item) {
                item.itemStyle = {normal: {label: {show: true, textStyle: {color: 'red'}}}};
            })
            $("#" + dataOption.container).append(chartView);
            var option = {
                title: {
                    text: dataOption.title,
                    subtext: dataOption.subTitle
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: dataOption.legend
                },
                dataZoom: {
                    show: dataOption.legend > 12 ? true : false,
                    start: 0
                },
                grid: {
                    y2: 40
                },
                toolbox: {
                    show: true,
                    feature: {
//                        mark: {show: true},
                        dataView: {show: true, readOnly: false},
                        magicType: {show: true, type: dataOption.viewType},
//                        restore: {show: true},
                        saveAsImage: {show: true}
                    }
                },
                calculable: true,
                xAxis: [
                    {
                        type: dataOption.xAxisType,
                        boundaryGap: dataOption.xAxisBoundaryGap,
                        data: dataOption.xAxisData
                    }
                ],
                yAxis: [
                    {
                        type: dataOption.yAxis,
                        axisLabel: {
                            formatter: dataOption.yAxisLabelFormatter
                        }
                    }
                ],
                series: dataOption.series
            };
            var myChart = echarts.init(chartView[0]);
            myChart.setOption(option);
        }


//        var printAble = true;
//        function print(fileName) {
//            html2canvas($("#totalTable"), {
//                onrendered: function (canvas) {
//                    $('#exportPic').attr('href', canvas.toDataURL());
//                    $('#exportPic').attr('download', fileName + '.png');
//                }
//            });
//            printAble = false;
//        }

    </script>
</head>
<body>
<nav class="breadcrumb">
    <i class="iconfont">󰄫</i> <a class="maincolor" href="${pageContext.request.contextPath}/">首页</a>
    <span class="c-999 en">&gt;</span><span class="c-666">数据仓库</span>
    <span class="c-999 en">&gt;</span><span class="c-666">行业库</span>
    <a class="btn btn-success radius r mr-20" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新"><i class="icon-refresh"></i></a>
</nav>
<div class="Hui-article">
    <div class="cl pl-20 pr-20 pt-10 pb-10">

        <h3>全局统计</h3>
        <div class="col-12 cl mt-20">
            <table>
                <tr>
                    <td style="vertical-align: top;">
                        <table>
                            <tr>
                                <td style="width: 200px;">已入驻院校数量</td>
                                <td>${universityCount}</td>
                            </tr>
                            <tr>
                                <td>编辑过个人简历的学生数量</td>
                                <td>${hasResumeCount}</td>
                            </tr>
                            <tr>
                                <td>独立注册学生账户数量</td>
                                <td>${registerNotImportStudentCount}</td>
                            </tr>
                            <tr>
                                <td>自行注册的企业数量</td>
                                <td>${registerCompanyCount}</td>
                            </tr>
                            <tr>
                                <td>平台企业总数</td>
                                <td>${companyCount}</td>
                            </tr>
                            <tr>
                                <td>独立注册的学生数量</td>
                                <td>${registerStudentCount}</td>
                            </tr>
                            <tr>
                                <td>发布职位的企业数量</td>
                                <td>${publishJobCompanyCount}</td>
                            </tr>
                            <tr>
                                <td>平台发布的职位数量</td>
                                <td>${jobCount}</td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td style="width: 100px;">已导入数据院校</td>
                                <td>
                                    <table>
                                        <tr>
                                            <td>院校代码</td>
                                            <td>院校名称</td>
                                            <td>导入数量</td>
                                        </tr>
                                        <c:set value="0" var="allImportCount"/>
                                        <c:forEach items="${importUniversityList}" var="uv">
                                            <tr>
                                                <td>${uv.code}</td>
                                                <td>${uv.name}</td>
                                                <td>${uv.count}</td>
                                            </tr>
                                            <c:set value="${allImportCount+uv.count}" var="allImportCount"/>
                                        </c:forEach>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td style="color: red;font-weight: 700;">总计:${allImportCount}</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

        </div>

        <div class="col-12 cl">
            <h3 class="mt-20">增长统计</h3>
        </div>
        <%--<c:if test="${empty startDate && empty endDate}">--%>
        <%
            DateTime end = new DateTime();
            DateTime start = end.minusMonths(3).toDateTime();
        %>
        <%--</c:if>--%>
        <div class="col-12 cl ">
            <div class="col-12 cl  pd-20" style="background: #f0f0f0;">
                <div class="col-9">
                    <div class="col-2 text-c">开始日期</div>
                    <div class="col-2">
                        <c:if test="${empty startDate && empty endDate}">
                            <input class="Wdate input-text" id="startDate" readonly="readonly" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" value="<%=start.toString("yyyy-MM-dd")%>"/>
                        </c:if>
                        <c:if test="${not empty startDate || not empty endDate}">
                            <input class="Wdate input-text" id="startDate" readonly="readonly" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" value="${startDate}"/>
                        </c:if>
                    </div>
                    <div class="col-2 text-c">结束日期</div>
                    <div class="col-2">
                        <c:if test="${empty startDate && empty endDate}">
                            <input class="Wdate input-text" id="endDate" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" value="<%=end.toString("yyyy-MM-dd")%>"/>
                        </c:if>
                        <c:if test="${not empty startDate || not empty endDate}">
                            <input class="Wdate input-text" id="endDate" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" value="${endDate}"/>
                        </c:if>
                    </div>
                    <div class="col-2 text-c">时间单位间隔类型</div>
                    <div class="col-2">
                        <select id="type" class="select">
                            <option value="week" selected="selected">周</option>
                            <option value="month">月</option>
                            <option value="quarter">季度</option>
                            <option value="year">年</option>
                        </select>
                    </div>
                </div>
                <div class="col-1"></div>
                <div class="col-2">
                    <button class="btn btn-primary" id="searchBtn">提交</button>
                </div>
            </div>
            <table>
                <tr>
                    <td id="chartContainer">
                    </td>
                </tr>
            </table>

        </div>
    </div>

</div>

</body>
</html>
