<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
  <title>首页</title>
</head>
<body>
<nav class="breadcrumb">
  <i class="iconfont">??</i> <a class="" href="${pageContext.request.contextPath}/">首页</a>
  <span class="c-999 en">&gt;</span><span class="c-666">企业库</span>
  <span class="c-gray en">&gt;</span> <c:if test="${empty model}">新增</c:if><c:if test="${not empty model}">编辑</c:if>
  <a class="btn btn-success radius r mr-20"
     style="line-height:1.6em;margin-top:3px" href="/div/company/list" title="返回">
    <i class="icon-reply"></i>
  </a>
</nav>
<div class="Hui-article">
  <article class="cl pl-20 pr-20 pt-10 pb-10">
    <form action="" method="post" class="form form-horizontal" id="form-company-add">
      <input type="hidden" name="orgCode" value="${model.orgCode}"/>
      <div class="row cl">
        <label class="form-label col-2"><span class="c-red">*</span>平台用户：</label>
        <div class="formControls col-5">
          <input type="text" class="input-text" data-validation-engine="validate[required,maxSize[30]]" value="${model.userCode}" placeholder="" id="userCode" name="userCode">
        </div>
        <div class="col-5"></div>

        <label class="form-label col-2"><span class="c-red">*</span>组织机构代码：</label>
        <label class="form-label col-2"><span class="c-red">*</span>全称：</label>
        <label class="form-label col-2">英文名：</label>
        <label class="form-label col-2">简称：</label>
        <label class="form-label col-2">认证情况：</label>
        <div class="formControls col-5 skin-minimal">
          <div class="radio-box">
            <input type="radio" id="publish-1" name="published" value="0"
                   <c:if test="${empty model or model.published == false}">checked="checked"</c:if>>
            <label for="publish-1">未认证(0)</label>
          </div>
          <div class="radio-box">
            <input type="radio" id="publish-2" name="published" value="1"
                   <c:if test="${not empty model and model.published == true}">checked="checked"</c:if>>
            <label for="publish-2">已认证(1)</label>
          </div>
        </div>
        <div class="col-5"></div>
        <label class="form-label col-2">规模：</label>
        <label class="form-label col-2">性质：</label>
        <label class="form-label col-2">行业大类：</label>
        <label class="form-label col-2">行业小类：</label>
        <label class="form-label col-2">福利（以逗号隔开）：</label>
        <label class="form-label col-2">企业简介：</label>
        <label class="form-label col-2">企业logo：</label>
        <label class="form-label col-2">联系人：</label>
        <label class="form-label col-2">联系电话：</label>
        <label class="form-label col-2">邮箱：</label>
        <label class="form-label col-2">企业官网：</label>
        <label class="form-label col-2">公司地址：</label>
        <label class="form-label col-2">经度：</label>
        <label class="form-label col-2">纬度：</label>
        <label class="form-label col-2">qq号码：</label>
        <label class="form-label col-2">二维码：</label>
        <label class="form-label col-2">微博：</label>
        <label class="form-label col-2">组织机构代码证：</label>
        <label class="form-label col-2">营业执照：</label>
        <label class="form-label col-2">税务登记证：</label>
        <label class="form-label col-2">社会信用代码：</label>
        <label class="form-label col-2">企业理念标语：</label>
        <div class="col-5"></div>
        <div class="row cl">
          <div class="col-7 col-offset-4">
            <input class="btn btn-primary radius" type="button" onclick="formSubmit();" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
            <input class="btn radius" type="button" onclick="location='${pageContext.request.contextPath}/dic/company/list'" value="&nbsp;&nbsp;返回&nbsp;&nbsp;">
          </div>
        </div>
      </div>>
    </form>
  </article>
</div>

<!-- Scripts -->
<script>
  $(function () {
    $('#form-company-add').validationEngine({promptPosition: "centerRight"});
  });

  function formSubmit() {
    if ($('#form-company-add').validationEngine("validate")) {
      $.ajax({
        url: '${pageContext.request.contextPath}/dic/company/save',
        type: 'post',
        data: $("#form-company-add").serialize(),
        async: false,
        cache: false,
        success: function (data) {
          if (data.ok == true) {
            layer.msg('保存成功!');
            window.location = '${pageContext.request.contextPath}/dic/company/list';
          } else {
            layer.msg('保存失败!');
          }
        },
        error: function () {
          layer.msg('保存失败!');
        }
      })
    }
  }

</script>
</body>
</html>