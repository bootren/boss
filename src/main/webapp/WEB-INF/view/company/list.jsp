
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
  <title>首页</title>
  <link href="${pageContext.request.contextPath}/res/lib/jquery.bootgrid-1.2.0/jquery.bootgrid.css" rel="stylesheet">
</head>
<body>
<nav class="breadcrumb">
  <i class="iconfont">󰄫</i> <a class="maincolor" href="${pageContext.request.contextPath}/">首页</a>
  <span class="c-999 en">&gt;</span><span class="c-666">数据仓库</span>
  <span class="c-999 en">&gt;</span><span class="c-666">企业库</span>
  <a class="btn btn-success radius r mr-20" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新"><i
          class="icon-refresh">刷新</i></a>
</nav>
<div class="Hui-article">
  <article class="cl pl-20 pr-20 pt-10 pb-10">
    <%--查询条件 如果有需要的话--%>
    <div class="text-c pt-20 pb-20">
      <%--发布日期范围：--%>
      <%--<input type="text" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'endDate\')||\'%y-%M-%d\'}'})" readonly id="startDate" class="input-text Wdate" style="width:120px;">--%>
      <%-----%>
      <%--<input type="text" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'startDate\')}',maxDate:'%y-%M-%d'})" readonly id="endDate" class="input-text Wdate" style="width:120px;">--%>
      <input type="text" class="input-text" style="width:250px"placeholder="请企业名称" id="searchInput" name="">
      <button type="button" class="btn btn-success" id="searchBtn" name=""><i class="icon-search"></i> 搜索企业名</button>
    </div>
    <div class="text-l pt-10">
      <a href="${pageContext.request.contextPath}/dic/company/edit" class="btn btn-primary radius"><i class="icon-plus"></i> 添加企业</a></span>
    </div>
    <%--表格--%>
      <table id="grid-data" class="table table-border table-striped table-hover mt-10">
      <thead>
      <tr>
        <%--<th data-column-id="id" data-type="numeric">ID</th>--%>
        <%--<th data-column-id="noticeNo" data-formatter="noticeNo">编号</th>--%>
        <th data-column-id="userCode;">平台用户</th>
        <%--<th data-column-id="published" data-formatter="published">状态</th>--%>
        <%--<th data-column-id="publishedTime">发布时间</th>--%>
          <th data-column-id="orgCode">组织机构代码</th>
          <th data-column-id="name">企业全称</th>
          <th data-column-id="enName">英文名</th>
          <th data-column-id="shortName ">简称</th>
          <%--<th data-column-id="state ">认证情况</th>--%>
          <%--<th data-column-id="state ">规模</th>--%>
          <%--<th data-column-id="state ">性质</th>--%>
          <%--<th data-column-id="state ">行业大类</th>--%>
          <%--<th data-column-id="state ">行业小类</th>--%>
          <%--<th data-column-id="state ">福利(多个以,分割)</th>--%>
          <%--<th data-column-id="state ">企业简介</th>--%>
          <%--<th data-column-id="state ">企业logo</th>--%>
          <%--<th data-column-id="state ">联系人</th>--%>
          <%--<th data-column-id="state ">联系电话</th>--%>
          <%--<th data-column-id="state ">邮箱</th>--%>
          <%--<th data-column-id="state ">企业官网</th>--%>
          <%--<th data-column-id="state ">公司地址</th>--%>
          <%--<th data-column-id="state ">经度</th>--%>
          <%--<th data-column-id="state ">纬度</th>--%>
          <%--<th data-column-id="state ">QQ号码</th>--%>
          <%--<th data-column-id="state ">二维码</th>--%>
          <%--<th data-column-id="state ">微博</th>--%>
          <%--<th data-column-id="state ">组织机构代码证</th>--%>
          <%--<th data-column-id="state ">营业执照（上传图片路径）</th>--%>
          <%--<th data-column-id="state ">税务登记证（上传图片路径）</th>--%>
          <%--<th data-column-id="state ">税务登记证（上传图片路径）</th>--%>
          <%--<th data-column-id="state ">企业理念标语</th>--%>
          <th data-column-id="commands" data-formatter="commands">操作</th>
      </tr>
      </thead>
    </table>
  </article>
</div>
<%--<span class="c-red">*</span>--%>
<!-- Scripts -->
<script src="${pageContext.request.contextPath}/res/lib/jquery.bootgrid-1.2.0/jquery.bootgrid.js"></script>
<%--<script src="/res/lib/My97DatePicker/WdatePicker.js"></script>--%>
<script>
  function searchGrid() {
    $("#grid-data").bootgrid("search", Math.random());
  }
  $(function () {
    //查询
    $("#searchBtn").click(function () {
      searchGrid();
    });
    //列表
    var grid = $("#grid-data").bootgrid({
      /***固定配置，一般不需要变化****/
      navigation: 2,
//            rowCount:20,//分页size
      ajax: true,//ajax访问方式
      sorting: false,//禁用列排序
      //转换传递参数以对应后端pageable对象（固定）
      requestHandler: function (request) {
        return pageUtil.requestHandler(request);
      },
      //转换结果以适应数据表格的输出（固定）
      responseHandler: function (response) {
        return pageUtil.responseHandler(response);
      },
      /***以下需要配置****/
      url: "${pageContext.request.contextPath}/dic/company/gridData",
      post: function () {//封装传递参数
        return {
          searchKey: $("#searchInput").val()
        };
      },
      formatters: {
        "commands": function (column, row) {
          return "<button title=\"查看\" type=\"button\" class=\"btn btn-xs btn-default command-view\" data-row-no=\"" + row.orgCode + "\"><span class=\"icon icon-edit\"></span></button> " +
                  "<button title=\"编辑\" type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-row-no=\"" + row.orgCode + "\"><span class=\"icon icon-edit\"></span></button> " +
                  "<button title=\"删除\" type=\"button\" class=\"btn btn-xs btn-default command-delete\" data-row-no=\"" + row.orgCode + "\"><span class=\"icon icon-trash\"></span></button>";
        }
      }
    }).on("loaded.rs.jquery.bootgrid", function () {
      //编写相关事件
//             find(".command-view").on("click", function (e) {
//                window.location = '/dic/company/view/' + $(this).data("row-no");
//            }).end().
      grid.find(".command-edit").on("click", function (e) {
        window.location = '${pageContext.request.contextPath}/dic/company/edit?orgCode=' + $(this).data("row-no");
      }).end().find(".command-view").on("click", function (e) {
                window.location = '${pageContext.request.contextPath}/dic/company/view/' + $(this).data("row-no");
      }).end().find(".command-delete").on("click", function (e) {
        var no = $(this).data("row-no");
        layer.confirm('确定要删除吗？', {
          btn: ['确定', '取消'], //按钮
          shade: false //不显示遮罩
        }, function () {
          $.post('${pageContext.request.contextPath}/dic/company/remove/' + no, function (data) {
            if (data && data.ok == true) {
              layer.msg("删除成功");
              searchGrid();
            }
          });
        }, function () {
        });
      });
    });
  });
</script>
</body>
</html>