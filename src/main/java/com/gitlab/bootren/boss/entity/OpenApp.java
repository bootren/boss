package com.gitlab.bootren.boss.entity;

import com.gitlab.bootren.data.core.jpa.entity.BaseEntity;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 开放平台app
 * Created by serv on 15/10/15.
 */
@Entity
@Table(name = "OPEN_APP")
public class OpenApp extends BaseEntity{

    /**
     * 应用id
     */
    @Column(name = "APP_ID", nullable = false,length = 32,unique = true,updatable = false)
    private String appId;

    /**
     * 应用密钥
     */
    @Column(name = "APP_SECRET", nullable = false,length = 32)
    private String appSecret;

    /**
     * 上一次的应用密钥
     */
    @Column(name = "OLD_APP_SECRET",length = 32)
    private String oldAppSecret;

    /**
     * 应用名称
     */
    @Column(name = "NAME", length = 255)
    private String name;

    /**
     * 描述
     */
    @Column(name = "DESCRIPTION", length = 255)
    private String description;

    /**
     * 应用图标url地址
     */
    @Column(name = "LOGO", length = 255)
    private String logo;

    /**
     * 应用网站地址
     */
    @Column(name = "URL", length = 255)
    private String url;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
        if(StringUtils.isEmpty(this.appSecret)){
            this.oldAppSecret = appSecret;
        }else{
            this.oldAppSecret = this.appSecret;
        }
    }

    public String getOldAppSecret() {
        return oldAppSecret;
    }

    @Deprecated
    public void setOldAppSecret(String oldAppSecret) {
        this.oldAppSecret = oldAppSecret;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
