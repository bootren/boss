package com.gitlab.bootren.boss.entity;

import com.gitlab.bootren.data.core.jpa.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 敏感词
 * Created by LiYonghui on 15/6/12.
 */
@Entity
@Table(name = "SENSITIVE_WORD")
public class SensitiveWord extends BaseEntity {
    /**
     * 敏感词
     */
    @Column(name = "NAME", length = 20, nullable = false)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
