package com.gitlab.bootren.boss.entity;

import com.gitlab.bootren.data.core.jpa.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by serv on 16/1/28.
 */
@Entity
@Table(name = "DEVICE_APP")
public class DeviceApp extends BaseEntity {

    @Column(name = "APP_ID",length = 32)
    private String appId;

    /**
     * app安装包的版本
     */
    @Column(name = "APP_VERSION",length = 12)
    private String appVersion;

    /**
     * app的安装包url地址
     */
    @Column(name = "APP_URL")
    private String appUrl;

    /**
     * 是否强制更新
     */
    @Column(name = "FORCED")
    private boolean forced;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getAppUrl() {
        return appUrl;
    }

    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl;
    }

    public boolean isForced() {
        return forced;
    }

    public boolean getForced() {
        return forced;
    }

    public void setForced(boolean forced) {
        this.forced = forced;
    }
}
