package com.gitlab.bootren.boss.entity;

import com.gitlab.bootren.data.core.jpa.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

/**
 * 区域
 * Created by LiYonghui on 15/6/26.
 */
@Entity
@Table(name = "AREA")
public class Area extends BaseEntity {
    /**
     * 上级区域
     */
    @Column(name = "PARENT_ID")
    private Integer parentId;
    /**
     * 区域名称
     */
    @Column(name = "NAME", nullable = false)
    private String name;

    //排序字段
    @Column(name = "SORT", nullable = false)
    private Integer sort;

    //保留
    @Column(name = "IS_PARENT",length = 11, nullable = false)
    private Integer isParent;

    //区域代码
    @Column(name = "AREA_CODE", nullable = false)
    private String areaCode;

    //
    /**
     * 区域类型
     * 1 省 2 市 3 县/区 4 乡/镇/街道
     */
    @Column(name = "AREA_TYPE",length = 255, nullable = false)
    private Integer areaType = 1;   //省




    //热门城市
    @Column(name = "HOTCITY",length = 255, nullable = false)
    private String hotcity;

    //拼音全拼
    @Column(name = "PINYIN", nullable = false)
    private String pinyin;

    //拼音简拼
    @Column(name = "PY",nullable = false)
    private String py;

    @Transient
    private List<Area> children;

    public Integer getAreaType() {
        return areaType;
    }

    public void setAreaType(Integer areaType) {
        this.areaType = areaType;
    }

    public String getPinyin() {
        return pinyin;
    }

    public void setPinyin(String pinyin) {
        this.pinyin = pinyin;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getIsParent() {
        return isParent;
    }

    public void setIsParent(Integer isParent) {
        this.isParent = isParent;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getHotcity() {
        return hotcity;
    }

    public void setHotcity(String hotcity) {
        this.hotcity = hotcity;
    }

    public String getPy() {
        return py;
    }

    public void setPy(String py) {
        this.py = py;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public List<Area> getChildren() {
        return children;
    }

    public void setChildren(List<Area> children) {
        this.children = children;
    }
}
