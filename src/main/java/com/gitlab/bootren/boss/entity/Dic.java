package com.gitlab.bootren.boss.entity;

import com.gitlab.bootren.data.core.jpa.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by LiYonghui on 15/7/17.
 */
@Entity
@Table(name = "DIC")
public class Dic extends BaseEntity {
    /**
     * 字典名称
     */
    @Column(name = "NAME", nullable = false)
    private String name;
    /**
     * 字典key
     */
    @Column(name = "DIC_CODE",length = 64,nullable = false)
    private String code;
    /**
     * 字典value
     */
    @Column(name = "DIC_VALUE", nullable = false)
    private String value;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
