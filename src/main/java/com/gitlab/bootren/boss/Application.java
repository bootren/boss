package com.gitlab.bootren.boss;

import com.gitlab.bootren.data.core.jpa.factory.PlatformJpaRepositoryFactoryBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

/**
 * Created by serv on 2015/6/11.
 */
@SpringBootApplication
@ImportResource(value = {"classpath:dubbo.xml"})
@EnableJpaRepositories(repositoryFactoryBeanClass = PlatformJpaRepositoryFactoryBean.class)
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class Application extends SpringBootServletInitializer{


    @Bean(name = "memberDataSource")
    @ConfigurationProperties(prefix="member")
    public DataSource memberDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "hrDataSource")
    @ConfigurationProperties(prefix="hr")
    public DataSource hrDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "rbacDataSource")
    @ConfigurationProperties(prefix="rbac")
    public DataSource rbacDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "memberJdbcTemplate")
    public JdbcTemplate memberJdbcTemplate(){
        return new JdbcTemplate(memberDataSource());
    }

    @Bean(name = "hrJdbcTemplate")
    public JdbcTemplate hrJdbcTemplate(){
        return new JdbcTemplate(hrDataSource());
    }

    @Bean(name = "rbacJdbcTemplate")
    public JdbcTemplate rbacJdbcTemplate(){
        return new JdbcTemplate(rbacDataSource());
    }


    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(Application.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class,args);
    }
}
