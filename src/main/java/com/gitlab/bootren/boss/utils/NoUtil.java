package com.gitlab.bootren.boss.utils;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 编号生成工具类
 * Created by LiYonghui on 15/6/26.
 */
public class NoUtil {
    /**
     * 按照指定前缀+时间戳+4位随机码的方式
     *
     * @param key
     * @return
     */
    public static String getNo(String key) {
        StringBuffer no = new StringBuffer();
        Date t = Calendar.getInstance().getTime();
        no.append(new SimpleDateFormat("yyyyMMddHHmmssSS").format(t));
        no.append(RandomUtils.nextInt(1000, 9999));
        if (StringUtils.isNotBlank(key)) {
            no.insert(0, key);
        }
        return no.toString();
    }

    public static String getNo() {
        return getNo(null);
    }

    public static void main(String args[]) {
        System.out.println(NoUtil.getNo(null));
        System.out.println(NoUtil.getNo("notice"));
    }
}
