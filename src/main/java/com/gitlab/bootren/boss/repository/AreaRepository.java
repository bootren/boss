package com.gitlab.bootren.boss.repository;

import com.gitlab.bootren.boss.entity.Area;
import com.gitlab.bootren.data.core.jpa.PlatformJpaRepository;

/**
 * @author LiYonghui
 */
public interface AreaRepository extends PlatformJpaRepository<Area, Integer> {
}
