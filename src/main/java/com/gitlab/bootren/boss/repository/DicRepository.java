package com.gitlab.bootren.boss.repository;

import com.gitlab.bootren.boss.entity.Dic;
import com.gitlab.bootren.data.core.jpa.PlatformJpaRepository;

/**
 * @author LiYonghui
 */
public interface DicRepository extends PlatformJpaRepository<Dic, Integer> {
}
