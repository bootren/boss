package com.gitlab.bootren.boss.repository;

import com.gitlab.bootren.boss.entity.SensitiveWord;
import com.gitlab.bootren.data.core.jpa.PlatformJpaRepository;

/**
 * @author LiYonghui
 */
public interface SensitiveWordRepository extends PlatformJpaRepository<SensitiveWord, Integer> {
}
