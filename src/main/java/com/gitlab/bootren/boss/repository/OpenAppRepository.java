package com.gitlab.bootren.boss.repository;

import com.gitlab.bootren.boss.entity.OpenApp;
import com.gitlab.bootren.data.core.jpa.PlatformJpaRepository;

/**
 * Created by serv on 15/10/15.
 */
public interface OpenAppRepository extends PlatformJpaRepository<OpenApp,Integer>{
}
