package com.gitlab.bootren.boss.repository;

import com.gitlab.bootren.boss.entity.DeviceApp;
import com.gitlab.bootren.data.core.jpa.PlatformJpaRepository;

/**
 * Created by serv on 16/1/28.
 */
public interface DeviceAppRepository extends PlatformJpaRepository<DeviceApp,Integer> {
}
