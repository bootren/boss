package com.gitlab.bootren.boss.web;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.subtlelib.poi.api.sheet.SheetContext;
import org.subtlelib.poi.api.workbook.WorkbookContext;
import org.subtlelib.poi.impl.workbook.WorkbookContextFactory;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by serv on 16/3/7.
 */
@Controller
@RequestMapping("/cc/st")
public class StudentReportController {

    private final static Logger LOGGER = LoggerFactory.getLogger(StudentReportController.class);


//    @Autowired
//    MongoTemplate mongoTemplate;

    @Autowired
    @Qualifier("hrJdbcTemplate")
    JdbcTemplate hrJdbcTemplate;

    @Autowired
    @Qualifier("memberJdbcTemplate")
    JdbcTemplate memberJdbcTemplate;

    @Autowired
    @Qualifier("rbacJdbcTemplate")
    JdbcTemplate rbacJdbcTemplate;

    @RequestMapping(value = "/{universityCode}")
    @ResponseBody
    public ResponseEntity<byte[]> studentInfo(@PathVariable String universityCode,String graduate) throws UnsupportedEncodingException {


        List<Map<String, Object>> colleges = memberJdbcTemplate.queryForList("SELECT c.*,m.name as campus from university_college c LEFT JOIN university_campus m on c.campus_id = m.`_id` WHERE c.university_code = " + universityCode);

        List<Map<String,Object>> regStudentCountList = hrJdbcTemplate.queryForList("select user_code,COUNT(_id) as reg_count from registration group by user_code");
        List<Map<String,Object>> deliveryStudentCountList = hrJdbcTemplate.queryForList("select user_code,COUNT(_id) as delivery_count from registration where is_delivery = 1 group by user_code");

        WorkbookContext workbook = WorkbookContextFactory.useXlsx().createWorkbook();

        for (Map college : colleges){

            List<Map<String, Object>> students = memberJdbcTemplate.queryForList("select r.code as resume,r.percentage as percentage,c.name as campus ,g.name as college,m.name as major , d.name as grade,u.name as university,s.* from edu_member.student s\n" +
                    "  LEFT JOIN edu_member.university u ON s.university_code = u.code\n" +
                    "  LEFT JOIN edu_member.university_campus c on s.campus_id = c.`_id`\n" +
                    "  LEFT JOIN edu_member.university_college g on s.college_id = g.`_id`\n" +
                    "  LEFT JOIN edu_member.university_major m on s.major_id = m.`_id`\n" +
                    "  LEFT JOIN edu_member.university_grade d on s.grade_id = d.`_id`\n" +
                    "  LEFT JOIN edu_hr.resume r on s.user_code = r.user_code\n" +
                    "where (year(s.graduate) = "+(graduate==null?"'2016'":("'"+graduate+"'"))+") and s.is_deleted = 0 and s.university_code = " + universityCode + " and s.college_id = " + college.get("_id"));

            if(students!=null&&students.size()>0){

                LOGGER.info(college.get("campus") + "-" + college.get("name")+college.get("_id"));

                SheetContext sheetCtx = workbook.createSheet(college.get("campus") + "-" + college.get("name")+college.get("_id"));

                sheetCtx
                        .nextRow()
//                .skipCell()
                        .header("序号")
                        .header("学生code")
                        .header("学号")
                        .header("学生名称")
                        .header("校区")
                        .header("学院")
                        .header("专业")
                        .header("班级")
                        .header("性别")
                        .header("入学时间")
                        .header("毕业时间")
//                        .header("是否登录过系统")
                        .header("是否编辑过简历")
                        .header("简历完善程度")
                        .header("报名数量")
                        .header("投递简历数量")
                ;

                for (int i=0;i<students.size();i++){
                    Map s = students.get(i);
                    final String usercode = (String) s.get("user_code");
//                    boolean recorded = mongoTemplate.count(Query.query(Criteria.where("usercode").is(usercode)), "recordAudit") > 0;

                    Map<String, Object> regMap = Iterables.find(regStudentCountList, new Predicate<Map<String, Object>>() {
                        @Override
                        public boolean apply(Map<String, Object> input) {
                            return input.get("user_code").equals(usercode);
                        }
                    },null);

                    Map<String,Object> deliveryMap = Iterables.find(deliveryStudentCountList, new Predicate<Map<String, Object>>() {
                        @Override
                        public boolean apply(Map<String, Object> input) {
                            return input.get("user_code").equals(usercode);
                        }
                    },null);

                    sheetCtx
                            .nextRow()
                            .number(i+1)
                            .text(s.get("code")!=null?(String)s.get("code"):"")
                            .text(s.get("sid")!=null?(String)s.get("sid"):"")
                            .text(s.get("name")!=null?(String)s.get("name"):"")
                            .text(s.get("campus")!=null?(String)s.get("campus"):"")
                            .text(s.get("college")!=null?(String)s.get("college"):"")
                            .text(s.get("major")!=null?(String)s.get("major"):"")
                            .text(s.get("grade")!=null?(String)s.get("grade"):"")
                            .text(s.get("gender")!=null?(String)s.get("gender"):"")
                            .text(s.get("enrolled")!=null?new DateTime(s.get("enrolled")).toString("yyyy"):"")
                            .text(s.get("graduate")!=null?new DateTime(s.get("graduate")).toString("yyyy"):"")

//                            .text(recorded?"是":"")//.text(recordService.isRecord(c.getUserCode())?"是":"否")
                            .text(s.get("resume")!=null?"是":"")
                            .text(s.get("percentage")!=null?s.get("percentage")+"%":"")
                            .text(regMap==null?"":String.valueOf(regMap.get("reg_count")))
                            .text(deliveryMap==null?"":String.valueOf(deliveryMap.get("delivery_count")))

                    ;


                }
            }


        }


        HttpHeaders headers = new HttpHeaders();
        String fileName=new String("学生统计信息.xlsx".getBytes("UTF-8"),"iso-8859-1");//为了解决中文名称乱码问题
        headers.setContentDispositionFormData("attachment", fileName);
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);

        ResponseEntity<byte[]> responseEntity = new ResponseEntity<byte[]>(workbook.toNativeBytes(),headers, HttpStatus.OK);

        return responseEntity;

    }

}
