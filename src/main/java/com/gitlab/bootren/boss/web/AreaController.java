package com.gitlab.bootren.boss.web;

import com.gitlab.bootren.boss.entity.Area;
import com.gitlab.bootren.boss.service.AreaServiceImpl;
import com.gitlab.bootren.common.web.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;


/**
 * 公告控制器
 * Created by LiYonghui on 15/6/29.
 */
@Controller
@RequestMapping("dic/area")
public class AreaController {

    @Autowired
    AreaServiceImpl areaService;

    /**
     * 转向管理页面
     *
     * @return
     */
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String list() {
        return "/area/list";
    }

    /**
     * 获取树形组件
     *
     * @return
     */
    @RequestMapping("treeData")
    @ResponseBody
    public List<Area> getTreeData() {
        return areaService.findAllArea();
    }


    /**
     * 保存
     *
     * @param area
     * @param model
     * @return
     */
    @RequestMapping(value = "save", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult save(Area area, HttpSession session, Model model) {
        Integer id = areaService.save(area);
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setObject(id);
        return ajaxResult;
    }

    /**
     * 删除
     *
     * @param id
     * @param session
     * @param model
     * @return
     */
    @RequestMapping(value = "remove/{id}", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult delete(@PathVariable Integer id, HttpSession session, Model model) {
        //假删除
        boolean flag = areaService.delete(id);
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setOk(flag);
        if (!flag) {
            ajaxResult.setMessage("原因：可能包含子分类。");
        }
        return ajaxResult;
    }

}
