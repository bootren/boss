package com.gitlab.bootren.boss.web;

import com.gitlab.bootren.boss.entity.Dic;
import com.gitlab.bootren.boss.service.DicServiceImpl;
import com.gitlab.bootren.common.web.AjaxResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * 字典控制器
 * Created by Liyonghui on 15/7/18.
 */
@Controller
@RequestMapping("dic/dictionary")
public class DicController {

    @Autowired
    DicServiceImpl dicService;

    /**
     * 转向列表
     *
     * @return
     */
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String list(Model model) {
        return "/dictionary/list";
    }

    /**
     * 获取列表数据
     *
     * @param pageable
     * @param sort
     * @param request
     * @return
     */
    @RequestMapping(value = "gridData", method = RequestMethod.POST)
    @ResponseBody
    public Page<Dic> getPage(Pageable pageable, String sort, HttpServletRequest request) {
        String code = StringUtils.trimToNull(request.getParameter("code"));
        return dicService.findPage(pageable, code);
    }

    /**
     * 获取字典组
     *
     * @return
     */
    @RequestMapping("treeData")
    @ResponseBody
    public List<Dic> getTreeData() {
        return dicService.findGroups();
    }

    /**
     * 字典值
     *
     * @param code
     * @return
     */
    @RequestMapping(value = "dicListByCode/{code}", method = RequestMethod.POST)
    @ResponseBody
    public List<Dic> getDicListByCode(@PathVariable String code) {
        return dicService.getDicList(code);
    }

    /**
     * 行业详情
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "view/{id}", method = RequestMethod.GET)
    public String view(@PathVariable Integer id, Model model) {
        model.addAttribute("model", dicService.findOne(id));
        return "/dictionary/view";
    }

    /**
     * 保存
     *
     * @param
     * @param model
     * @return
     */
    @RequestMapping(value = "save", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult save(Dic dic, HttpSession session, Model model) throws Exception {
        Dic vo = dicService.findOne(dicService.save(dic));
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setObject(vo);
        return ajaxResult;
    }

    /**
     * 删除
     *
     * @param id
     * @param session
     * @param model
     * @return
     */
    @RequestMapping(value = "remove/{id}", method = RequestMethod.POST)
    @ResponseBody

    public AjaxResult delete(@PathVariable Integer id, HttpSession session, Model model) throws Exception {
        //假删除
        boolean flag = false;
        dicService.delete(id);
        flag = true;
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setOk(flag);
        return ajaxResult;
    }
}
