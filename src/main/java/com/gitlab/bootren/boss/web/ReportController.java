package com.gitlab.bootren.boss.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

/**
 * Created by serv on 16/1/14.
 */
@Controller
@RequestMapping("/report")
public class ReportController implements CountSQLConstants {


    @Autowired
    @Qualifier("hrJdbcTemplate")
    JdbcTemplate hrJdbcTemplate;

    @Autowired
    @Qualifier("memberJdbcTemplate")
    JdbcTemplate memberJdbcTemplate;

    @Autowired
    @Qualifier("rbacJdbcTemplate")
    JdbcTemplate rbacJdbcTemplate;


    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public String info(Model model) {
        model.addAttribute("universityCount", memberJdbcTemplate.queryForLong(universityCountSQL));
        model.addAttribute("importUniversityList", memberJdbcTemplate.queryForList(importUniversityListSQL));
        model.addAttribute("hasResumeCount", hrJdbcTemplate.queryForLong(hasResumeCountSQL));
        model.addAttribute("hasResumeStudentCount", hrJdbcTemplate.queryForLong(hasResumeStudentCountSQL));
        model.addAttribute("registerNotImportStudentCount", rbacJdbcTemplate.queryForLong(registerNotImportStudentCountSQL));
        model.addAttribute("registerCompanyCount", rbacJdbcTemplate.queryForLong(registerCompanyCountSQL));
        model.addAttribute("companyCount", rbacJdbcTemplate.queryForLong(companyCountSQL));
        model.addAttribute("registerStudentCount", rbacJdbcTemplate.queryForLong(registerStudentCountSQL));
        model.addAttribute("publishJobCompanyCount", hrJdbcTemplate.queryForLong(publishJobCompanyCountSQL));
        model.addAttribute("jobCount", hrJdbcTemplate.queryForLong(jobCountSQL));

        return "/report/info";
    }

    /**
     * 获取统计数据 for chart
     *
     * @param type      查询的周期间隔类型(week month quarter year) 最小为周
     * @param startDate 开始日期
     * @param endDate   结束日期
     * @return
     */
    @RequestMapping(value = "/info/data", method = RequestMethod.POST)
    @ResponseBody
    public List getInfoData(@RequestParam(required = false) String type, @RequestParam(required = false) String startDate, @RequestParam(required = false) String endDate, Model model) {
//                id: "test",
//                height: 300,
//        title: "图表名称",
//                subTitle: "副标题",
//                legend: ["图例1", "图例2"],
//        xAxisType: "category",
//                xAxisBoundaryGap: false,
//                xAxisData: ['周一', '周二', '周三', '周四', '周五', '周六', '周日'],
//        yAxisType: "value",
//                yAxisLabelFormatter: '{value} 单位',
//                series: [
//        {
//            name: '最高气温',
//                    type: 'line',
//                data: [11, 11, 15, 13, 12, 13, 10]
//        },
//        {
//            name: '最低气温',
//                    type: 'line',
//                data: [1, -2, 2, 5, 3, 2, 0]
//        }
//        ]
        String typeView = "周";
        if (type.equals("month")) {
            typeView = "月";
        }
        if (type.equals("quarter")) {
            typeView = "季度";
        }
        if (type.equals("year")) {
            typeView = "年";
        }
        List result = new ArrayList();

        //院校增长趋势
        String sql001 = groupUniversityCountSQL.replaceAll("#type#", type).replaceAll("#startDate#", startDate).replaceAll("#endDate#", endDate);
        List<Map<String, Object>> data001 = memberJdbcTemplate.queryForList(sql001);
        result.add(createMap(data001, "院校增长趋势", "院校数量", typeView));

        //完善个人简历趋势
        String sql002 = groupHasResumeCountSQL.replaceAll("#type#", type).replaceAll("#startDate#", startDate).replaceAll("#endDate#", endDate);
        List<Map<String, Object>> data002 = hrJdbcTemplate.queryForList(sql002);
        result.add(createMap(data002, "完善个人简历趋势", "简历数量", typeView));


        //完善个人简历的学生数量趋势
        String sql003 = groupHasResumeStudentCountSQL.replaceAll("#type#", type).replaceAll("#startDate#", startDate).replaceAll("#endDate#", endDate);
        List<Map<String, Object>> data003 = hrJdbcTemplate.queryForList(sql003);
        result.add(createMap(data003, "完善个人简历的学生数量趋势", "学生简历数量", typeView));

        //独立注册非导入学生账户数量
        String sql004 = groupRegisterNotImportStudentCountSQL.replaceAll("#type#", type).replaceAll("#startDate#", startDate).replaceAll("#endDate#", endDate);
        List<Map<String, Object>> data004 = rbacJdbcTemplate.queryForList(sql004);
        result.add(createMap(data004, "独立注册非导入学生账户数量趋势", "学生账户数量", typeView));

        //自行注册的企业数量趋势
        String sql005 = groupRegisterCompanyCountSQL.replaceAll("#type#", type).replaceAll("#startDate#", startDate).replaceAll("#endDate#", endDate);
        List<Map<String, Object>> data005 = rbacJdbcTemplate.queryForList(sql005);
        result.add(createMap(data005, "自行注册的企业数量趋势", "企业数量", typeView));


        //平台企业总数
        String sql006 = groupCompanyCountSQL.replaceAll("#type#", type).replaceAll("#startDate#", startDate).replaceAll("#endDate#", endDate);
        List<Map<String, Object>> data006 = rbacJdbcTemplate.queryForList(sql006);
        result.add(createMap(data006, "平台企业总数趋势", "企业数量", typeView));


        //独立注册的学生数量
        String sql007 = groupRegisterStudentCountSQL.replaceAll("#type#", type).replaceAll("#startDate#", startDate).replaceAll("#endDate#", endDate);
        List<Map<String, Object>> data007 = rbacJdbcTemplate.queryForList(sql007);
        result.add(createMap(data007, "独立注册的学生数量趋势", "学生数量", typeView));


        //发布职位的企业数量趋势
        String sql008 = groupPublishJobCompanyCountSQL.replaceAll("#type#", type).replaceAll("#startDate#", startDate).replaceAll("#endDate#", endDate);
        List<Map<String, Object>> data008 = hrJdbcTemplate.queryForList(sql008);
        result.add(createMap(data008, "发布职位的企业数量趋势", "企业数量", typeView));


        //独立注册的学生数量
        String sql009 = groupJobCountSQL.replaceAll("#type#", type).replaceAll("#startDate#", startDate).replaceAll("#endDate#", endDate);
        List<Map<String, Object>> data009 = hrJdbcTemplate.queryForList(sql009);
        result.add(createMap(data009, "平台发布的职位数量趋势", "职位数量", typeView));

        return result;
    }

    private Map<String, Object> createMap(List<Map<String, Object>> list, String title, String legend, String typeView) {

        List xData = new ArrayList();//x轴
        List yData = new ArrayList();//y轴

        long total = 0;

        for (Map<String, Object> data : list) {
            xData.add(data.get("year") + "年" + (typeView.equals("年") ? "" : data.get("type") + typeView));
            yData.add(data.get("count"));
            total += (Long) data.get("count");
        }

        //获取院校增长数据
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("id", UUID.randomUUID().toString());//id需要唯一
        map.put("title", title + "（共 " + total + "）");//主标题
        map.put("legend", new String[]{legend});//图例
        map.put("xAxisData", xData);//x轴 (x与值个数需要一致)

        Map<String, Object> seriesMap = new HashMap<String, Object>();
        seriesMap.put("name", legend);
        seriesMap.put("type", "line");
        seriesMap.put("data", yData);

        map.put("series", new Map[]{seriesMap});//值集合

        return map;
    }

}
