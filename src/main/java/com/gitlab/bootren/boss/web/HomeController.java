package com.gitlab.bootren.boss.web;

import com.gitlab.bootren.common.web.record.RecordAudit;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by LiYonghui on 15/6/16.
 */
@Controller
public class HomeController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(){
        return "redirect:/home";
    }

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String home(HttpSession session, Model model) {
        List<String> sys = new ArrayList<String>();
        sys.add(System.getProperty("java.version"));
        sys.add(System.getProperty("java.vendor"));
        sys.add(System.getProperty("java.vendor.url"));
        sys.add(System.getProperty("java.home"));
        sys.add(System.getProperty("java.vm.specification.version"));
        sys.add(System.getProperty("java.vm.specification.vendor"));
        sys.add(System.getProperty("java.vm.specification.name"));
        sys.add(System.getProperty("java.vm.version"));
        sys.add(System.getProperty("java.vm.vendor"));
        sys.add(System.getProperty("java.vm.name"));
        sys.add(System.getProperty("java.specification.version"));
        sys.add(System.getProperty("java.specification.vendor"));
        sys.add(System.getProperty("java.specification.name"));
        sys.add(System.getProperty("java.class.version"));
        sys.add(System.getProperty("java.class.path"));
        sys.add(System.getProperty("java.library.path"));
        sys.add(System.getProperty("java.io.tmpdir"));
        sys.add(System.getProperty("java.compiler"));
        sys.add(System.getProperty("java.ext.dirs"));

        sys.add(System.getProperty("os.name"));
        sys.add(System.getProperty("os.arch"));
        sys.add(System.getProperty("os.version"));
        sys.add(System.getProperty("file.separator"));
        sys.add(System.getProperty("path.separator"));
        sys.add(System.getProperty("line.separator"));
        sys.add(System.getProperty("user.name"));
        sys.add(System.getProperty("user.home"));
        sys.add(System.getProperty("user.dir"));


        model.addAttribute("sys", sys);
        return "home";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(HttpSession session) {
        return "/login";
    }

    @RecordAudit("登录后台主页")
    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public String welcome(HttpSession session, HttpServletRequest request, Model model) {
        List<String> sys = new ArrayList<String>();
        sys.add(System.getProperty("java.version"));
        sys.add(System.getProperty("java.vendor"));
        sys.add(System.getProperty("java.vendor.url"));
        sys.add(System.getProperty("java.home"));
        sys.add(System.getProperty("java.vm.specification.version"));
        sys.add(System.getProperty("java.vm.specification.vendor"));
        sys.add(System.getProperty("java.vm.specification.name"));
        sys.add(System.getProperty("java.vm.version"));
        sys.add(System.getProperty("java.vm.vendor"));
        sys.add(System.getProperty("java.vm.name"));
        sys.add(System.getProperty("java.specification.version"));
        sys.add(System.getProperty("java.specification.vendor"));
        sys.add(System.getProperty("java.specification.name"));
        sys.add(System.getProperty("java.class.version"));
        sys.add(System.getProperty("java.class.path"));
        sys.add(System.getProperty("java.library.path"));
        sys.add(System.getProperty("java.io.tmpdir"));
        sys.add(System.getProperty("java.compiler"));
        sys.add(System.getProperty("java.ext.dirs"));

        sys.add(System.getProperty("os.name"));
        sys.add(System.getProperty("os.arch"));
        sys.add(System.getProperty("os.version"));
        sys.add(System.getProperty("file.separator"));
        sys.add(System.getProperty("path.separator"));
        sys.add(System.getProperty("line.separator"));
        sys.add(System.getProperty("user.name"));
        sys.add(System.getProperty("user.home"));
        sys.add(System.getProperty("user.dir"));


        model.addAttribute("sys", sys);
        return "welcome";
    }

    @RequestMapping("component")
    public String component() {
        return "component";
    }

    @RequestMapping("/unauthorized")
    public String unauthorized() {
        return "unauthorized";
    }

}
