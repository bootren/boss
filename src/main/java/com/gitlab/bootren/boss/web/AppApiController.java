package com.gitlab.bootren.boss.web;

import com.gitlab.bootren.boss.entity.DeviceApp;
import com.gitlab.bootren.boss.service.OpenAppServiceImpl;
import com.gitlab.bootren.common.web.AjaxResult;
import com.gitlab.bootren.common.web.record.RecordAudit;
import com.gitlab.bootren.sign.SignApi;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by serv on 15/10/21.
 */
@RestController
@RequestMapping(value = "/api")
public class AppApiController {

    @Autowired
    OpenAppServiceImpl openAppService;

    /**
     * 获取版本更新信息
     *
     * @return
     */
    @RecordAudit("获取版本更新信息")
    @SignApi
    @RequestMapping(value = "/app/version", method = RequestMethod.POST)
    public AjaxResult getVersion(@RequestBody Map params) {
        String version = (String) params.get("version");
        String appId = (String) params.get("appId");
        String test = (String) params.get("test");

        AjaxResult result = new AjaxResult(true,"operation success");

        //判断appId 和 version 设置需要更新的版本号,是否强制和下载url
        DeviceApp deviceAppByAppId = openAppService.findDeviceAppByAppId(appId);
        if((deviceAppByAppId != null&&!StringUtils.equals(deviceAppByAppId.getAppVersion(),version))||StringUtils.equals(test,"true")){
            AppVersion appVersion = new AppVersion();
            appVersion.setVersion(deviceAppByAppId.getAppVersion());
            appVersion.setUrl(deviceAppByAppId.getAppUrl());
            appVersion.setForced(deviceAppByAppId.getForced());
            result.setObject(appVersion);
        }
        return result;
    }

    private class AppVersion{
        private String version;
        private String url;
        private boolean forced;

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public boolean isForced() {
            return forced;
        }

        public void setForced(boolean forced) {
            this.forced = forced;
        }
    }


}
