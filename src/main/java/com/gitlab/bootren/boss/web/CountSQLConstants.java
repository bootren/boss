package com.gitlab.bootren.boss.web;

/**
 * Created by serv on 16/1/25.
 */
public interface CountSQLConstants {

    //已入驻院校：
    String universityCountSQL = "SELECT count(*) FROM university WHERE is_deleted = 0 and code <> '2'";
    String groupUniversityCountSQL = "SELECT year(create_time) as year,#type#(create_time) as type, count(*) as count FROM university " +
            "where is_deleted = 0 and code <> '2' and create_time >= '#startDate#' and create_time <= '#endDate#' " +
            "GROUP BY #type#(create_time) order by create_time asc";

    //已导入数据院校：
    String importUniversityListSQL = "SELECT u.code,u.name,count(*) as count FROM student s LEFT JOIN university u on s.university_code = u.code WHERE s.is_deleted = 0 and u.is_deleted = 0 and u.code <> '2' and u.code <> '1' GROUP BY s.university_code";

    //平台编辑过个人简历的数量：
    String hasResumeCountSQL = "SELECT count(*) from resume where is_deleted = 0";
    String groupHasResumeCountSQL = "SELECT year(create_time) as year,#type#(create_time) as type,count(*) as count from resume " +
            "where is_deleted = 0 and create_time >= '#startDate#' and create_time <= '#endDate#' group by #type#(create_time) order by create_time asc";

    //编辑过个人简历的学生数量：
    String hasResumeStudentCountSQL = "SELECT count(*) from resume where is_deleted = 0 and user_code not like '%.%'";
    String groupHasResumeStudentCountSQL = "SELECT year(create_time) as year,#type#(create_time) as type,count(*) as count from resume " +
            "where is_deleted = 0 and user_code not like '%.%' and create_time >= '#startDate#' and create_time <= '#endDate#' group by #type#(create_time) order by create_time asc";

    //独立注册非导入学生账户数量：
    String registerNotImportStudentCountSQL = "SELECT count(*) from user where type = 'student'  and user.is_import = 0 and user.is_deleted = 0 and user.enable = 1";
    String groupRegisterNotImportStudentCountSQL = "SELECT year(create_time) as year,#type#(create_time) as type,count(*) as count from user " +
            "where type = 'student'  and user.is_import = 0 and user.is_deleted = 0 and user.enable = 1 " +
            "and create_time >= '#startDate#' and create_time <= '#endDate#' group by #type#(create_time) order by create_time asc";

    //自行注册的企业数量：
    String registerCompanyCountSQL = "SELECT count(*) FROM user where is_deleted = 0 and  type = 'company' and enable = 1 and user.is_import = 0";
    String groupRegisterCompanyCountSQL = "SELECT year(create_time) as year,#type#(create_time) as type,count(*) as count FROM user " +
            "where is_deleted = 0 and  type = 'company' and enable = 1 and user.is_import = 0 " +
            "and create_time >= '#startDate#' and create_time <= '#endDate#' group by #type#(create_time) order by create_time asc";

    //平台企业总数:
    String companyCountSQL = "SELECT count(*) FROM user where is_deleted = 0 and  type = 'company' and enable = 1";
    String groupCompanyCountSQL = "SELECT year(create_time) as year,#type#(create_time) as type,count(*) as count FROM user " +
            "where is_deleted = 0 and  type = 'company' and enable = 1 " +
            "and create_time >= '#startDate#' and create_time <= '#endDate#' group by #type#(create_time) order by create_time asc";

    //独立注册的学生数量：
    String registerStudentCountSQL = "SELECT count(*) from user where type = 'student'  and user.is_import = 0 and user.is_deleted = 0 and user.enable = 1";
    String groupRegisterStudentCountSQL = "SELECT year(create_time) as year,#type#(create_time) as type,count(*) as count from user " +
            "where type = 'student'  and user.is_import = 0 and user.is_deleted = 0 and user.enable = 1 " +
            "and create_time >= '#startDate#' and create_time <= '#endDate#' group by #type#(create_time) order by create_time asc";

    //发布职位的企业数量：
    String publishJobCompanyCountSQL = "SELECT count(company_code) from job where company_code like 'CO%' and is_deleted = 0";
    String groupPublishJobCompanyCountSQL = "SELECT year(create_time) as year,#type#(create_time) as type,count(*) as count from job " +
            "where company_code like 'CO%' and is_deleted = 0 " +
            "and create_time >= '#startDate#' and create_time <= '#endDate#' group by #type#(create_time) order by create_time asc";

    //平台发布的职位数量：
    String jobCountSQL = "SELECT count(*) from job where is_deleted = 0";
    String groupJobCountSQL = "SELECT year(create_time) as year,#type#(create_time) as type,count(*) as count from job " +
            "where is_deleted = 0 and create_time >= '#startDate#' and create_time <= '#endDate#' group by #type#(create_time) order by create_time asc";

    
}
