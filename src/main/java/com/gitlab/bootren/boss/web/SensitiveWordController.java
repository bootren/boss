package com.gitlab.bootren.boss.web;

import com.gitlab.bootren.boss.entity.SensitiveWord;
import com.gitlab.bootren.boss.service.SensitiveWordServiceImpl;
import com.gitlab.bootren.common.web.AjaxResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * 敏感词控制器
 * Created by LiYonghui on 15/6/29.
 */
@Controller
@RequestMapping("security/sensitive")
public class SensitiveWordController {

    @Autowired
    SensitiveWordServiceImpl sensitiveWordService;

    /**
     * 转向敏感词管理页面
     *
     * @return
     */
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String list() {
        return "/sensitive/list";
    }

    /**
     * 获取列表数据
     *
     * @param pageable
     * @param sort
     * @param request
     * @return
     */
    @RequestMapping(value = "gridData", method = RequestMethod.POST)
    @ResponseBody
    public Page<SensitiveWord> getPage(Pageable pageable, String sort, HttpServletRequest request) {
        String searchKey = StringUtils.trimToNull(request.getParameter("searchKey"));
        Map<String, Object> map = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(searchKey)) {
            map.put("searchKey", searchKey);
        }
        return sensitiveWordService.findPage(pageable, map);
    }


    /**
     * 转向编辑页面
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "edit", method = RequestMethod.GET)
    public String edit(@RequestParam(required = false) Integer id, Model model) {
        model.addAttribute("model", sensitiveWordService.getSensitiveWord(id));
        return "/sensitive/edit";
    }


    /**
     * 保存
     *
     * @param sensitiveWord
     * @param model
     * @return
     */
    @RequestMapping(value = "save", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult save(SensitiveWord sensitiveWord, HttpSession session, Model model) {
        //获取当前关键词
        String userCode = "";
        if (sensitiveWord.getId() == null) {

        } else {
            //编辑保存敏感词

        }
        sensitiveWordService.save(sensitiveWord);
        return new AjaxResult();
    }

    /**
     * 删除
     *
     * @param id
     * @param session
     * @param model
     * @return
     */
    @RequestMapping(value = "remove/{id}", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult delete(@PathVariable Integer id, HttpSession session, Model model) {
        //假删除
        sensitiveWordService.delete(id);
        return new AjaxResult();
    }


}

