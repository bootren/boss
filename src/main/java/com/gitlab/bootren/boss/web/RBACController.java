package com.gitlab.bootren.boss.web;

import com.gitlab.bootren.common.web.record.RecordAudit;
import com.google.common.collect.Lists;
import com.gitlab.bootren.common.utils.PageConvertor;
import com.gitlab.bootren.common.web.AjaxResult;
import com.gitlab.bootren.rbac.remote.ResourceService;
import com.gitlab.bootren.rbac.remote.RoleService;
import com.gitlab.bootren.rbac.remote.UserService;
import com.gitlab.bootren.rbac.remote.vo.ResourceVo;
import com.gitlab.bootren.rbac.remote.vo.RoleVo;
import com.gitlab.bootren.vo.page.PageVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 公告控制器
 * Created by LiYonghui on 15/6/29.
 */
@Controller
@RequestMapping("rbac")
public class RBACController {

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private ResourceService resourceService;
    @Autowired
    private PageConvertor pageConvertor;


    /**
     * 转向资源管理页面
     *
     * @return
     */
    @RecordAudit("转向资源管理页面")
    @RequestMapping(value = "resource/list", method = RequestMethod.GET)
    public String resourceList() {
        return "rbac/resource_list";
    }

    /**
     * 获取树形组件
     *
     * @return
     */
    @RecordAudit("获取资源树")
    @RequestMapping("resource/treeData")
    @ResponseBody
    public List<ResourceVo> getTreeData() {
        List<ResourceVo> resourceVos = resourceService.listResources();

        //添加跟节点
        ResourceVo root = new ResourceVo();
        root.setName("资源列表");
        root.setCode("root");
        root.setType("root");
        resourceVos.add(root);
        return resourceVos;
    }

    /**
     * 转向资源编辑页面
     *
     * @param code
     * @param model
     * @return
     */
    @RecordAudit("转向资源编辑页面")
    @RequestMapping(value = "resource/edit", method = RequestMethod.GET)
    public String resourceEdit(@RequestParam(required = false) String code, Model model) {
        if (StringUtils.isNotEmpty(code)) {
            model.addAttribute("model", resourceService.getResourceVoByCode(code));
        }
        return "rbac/resource_edit";
    }

    /**
     * 保存
     *
     * @param resource
     * @return
     */
    @RecordAudit("保存资源")
    @RequestMapping(value = "resource/save", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult resourceSave(ResourceVo resource) {
        resource = resourceService.saveResource(resource);
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setObject(resource);
        return ajaxResult;
    }

    /**
     * 删除
     *
     * @param code
     * @param model
     * @return
     */
    @RecordAudit("删除资源")
    @RequestMapping(value = "resource/remove/{code}", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult resourceDelete(@PathVariable String code, Model model) {
        resourceService.deleteResourceByCode(code);
        return new AjaxResult();
    }


    /**
     * 角色资源配置
     *
     * @param code
     * @param model
     * @return
     */
    @RecordAudit("转向角色资源关系配置")
    @RequestMapping(value = "role/resource", method = RequestMethod.GET)
    public String roleResource(@RequestParam String code, Model model) {
        return "rbac/role_resource";
    }

    /**
     * 获取角色对应的资源code列表
     *
     * @return
     */
    @RecordAudit("根据角色获取资源code列表")
    @RequestMapping(value = "role/resourceCodeList", method = RequestMethod.POST)
    @ResponseBody
    public List<String> getRoleGrid(String roleCode) {
        return roleService.getRoleByCode(roleCode).getResourceCodeList();
    }


    /**
     * 授权
     *
     * @param roleCode
     * @param res
     * @param model
     * @return
     */
    @RecordAudit("角色资源关系保存")
    @RequestMapping(value = "role/grant", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult grant(String roleCode, @RequestParam("res[]") String[] res, Model model) {
        RoleVo role = roleService.getRoleByCode(roleCode);
        role.setResourceCodeList(Lists.newArrayList(res));
        roleService.saveRole(role);
        return new AjaxResult();
    }


    /**
     * 转向角色列表页面
     *
     * @return
     */
    @RecordAudit("转到角色列表")
    @RequestMapping(value = "role/list", method = RequestMethod.GET)
    public String roleList(Model model) {
        return "rbac/role_list";
    }

    /**
     * 获取角色列表数据
     *
     * @param pageable
     * @param searchKey
     * @param request
     * @return
     */
    @RecordAudit("获取角色列表数据")
    @RequestMapping(value = "role/gridData", method = RequestMethod.POST)
    @ResponseBody
    public PageVo<RoleVo> getRoleGrid(Pageable pageable, String searchKey, HttpServletRequest request) {
        return roleService.listRoles(pageConvertor.convert2PageableVo(pageable), searchKey);
    }

    /**
     * 转向编辑页面
     *
     * @param code
     * @param model
     * @return
     */
    @RecordAudit("转向角色编辑页面")
    @RequestMapping(value = "role/edit", method = RequestMethod.GET)
    public String roleEdit(@RequestParam(required = false) String code, Model model) {
        if (StringUtils.isNotEmpty(code)) {
            model.addAttribute("model", roleService.getRoleByCode(code));
        }
        return "rbac/role_edit";
    }

    /**
     * 保存角色
     *
     * @param role
     * @return
     */
    @RecordAudit("保存角色")
    @RequestMapping(value = "role/save", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult roleSave(RoleVo role) {
        RoleVo byCode = roleService.getRoleByCode(role.getCode());
        if (byCode == null) {
            byCode = new RoleVo();
        }
        byCode.setType("system");
        byCode.setCode(role.getCode());
        byCode.setValue(role.getValue());
        byCode.setName(role.getName());
        roleService.saveRole(byCode);
        return new AjaxResult();
    }

    /**
     * 删除
     *
     * @param code
     * @param model
     * @return
     */
    @RecordAudit("删除角色")
    @RequestMapping(value = "role/remove/{code}", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult roleDelete(@PathVariable String code, Model model) {
        roleService.deleteRoleByCode(code);
        return new AjaxResult();
    }
}
