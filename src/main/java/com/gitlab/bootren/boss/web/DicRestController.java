package com.gitlab.bootren.boss.web;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.gitlab.bootren.boss.handler.AreaDic;
import com.gitlab.bootren.boss.handler.DicDic;
import com.gitlab.bootren.boss.service.AreaServiceImpl;
import com.gitlab.bootren.boss.service.DicServiceImpl;
import com.gitlab.bootren.common.utils.BeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 字典rest控制器（为各类公共字典调用）
 * Created by Liyonghui on 15/7/18.
 */
@ConditionalOnWebApplication
@Controller
@RequestMapping("rest/dict")
public class DicRestController {

    @Autowired
    AreaServiceImpl areaService;
    @Autowired
    DicServiceImpl dicService;
    @Autowired
    BeanMapper beanMapper;

    /**
     * 获取区域结果
     *
     * @return
     */
    @RequestMapping(value = "area", method = RequestMethod.GET)
    @ResponseBody
    public JSONPObject findAreas(HttpServletRequest request, HttpServletResponse response) {
        String callback = request.getParameter("callback");
        return new JSONPObject(callback, beanMapper.mapAsList(areaService.findAllArea(), AreaDic.class));
    }



    /**
     * @param code
     * @param response
     * @return
     */
    @RequestMapping(value = "dic/{code}", method = RequestMethod.GET)
    @ResponseBody
    public JSONPObject findDicByCode(@PathVariable String code, HttpServletRequest request, HttpServletResponse response) {
        String callback = request.getParameter("callback");
        return new JSONPObject(callback, beanMapper.mapAsList(dicService.getDicList(code), DicDic.class));

    }

}
