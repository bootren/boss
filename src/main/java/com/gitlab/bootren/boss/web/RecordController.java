package com.gitlab.bootren.boss.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.subtlelib.poi.api.sheet.SheetContext;
import org.subtlelib.poi.api.workbook.WorkbookContext;
import org.subtlelib.poi.impl.workbook.WorkbookContextFactory;

import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

/**
 * Created by serv on 16/3/21.
 */
@Controller
public class RecordController {

    public final static String RECORD_COLLECTION_NAME = "recordAudit";

    @Autowired
    MongoTemplate mongoTemplate;
    @Autowired
    ObjectMapper objectMapper;

    //转向日志列表页面
    @RequestMapping(value = "/record/list",method = RequestMethod.GET)
    public String gotoList(){
        return "/record/list";
    }

    @RequestMapping(value = "/record/list/gridData",method = RequestMethod.POST)
    @ResponseBody
    public Page<Map> recordGridList(Pageable pageable,String searchKey) throws JsonProcessingException {

        pageable = new PageRequest(pageable.getPageNumber(),pageable.getPageSize(), Sort.Direction.DESC,"startTime");

        Criteria criteria = new Criteria();
        if(StringUtils.isNotEmpty(searchKey)){
            criteria = criteria.orOperator(
                    Criteria.where("usercode").regex(".*?"+searchKey+".*"),
                    Criteria.where("username").regex(".*?"+searchKey+".*"),
                    Criteria.where("url").regex(".*?"+searchKey+".*"),
                    Criteria.where("function").regex(".*?"+searchKey+".*")
            );
        }

        Query query = Query.query(criteria).with(pageable);

        long count = mongoTemplate.count(query, Map.class, RECORD_COLLECTION_NAME);

        List<Map> maps = mongoTemplate.find(query, Map.class, RECORD_COLLECTION_NAME);

        for (Map map : maps){
            try{
                DateTime startTime = DateTime.parse((String) map.get("startTime"), DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
                DateTime endTime = DateTime.parse((String) map.get("endTime"), DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));

                map.put("time",endTime.getMillis()-startTime.getMillis());
            }catch (Exception e){
                map.put("time","-");
            }
            map.put("toolTip",objectMapper.writeValueAsString(map));

        }

        return new PageImpl<Map>(maps,pageable,count);
    }


    @RequestMapping(value = "/record/download",method = RequestMethod.GET)
    public ResponseEntity<byte[]> downloadLogs(String searchKey){


        Criteria criteria = new Criteria();
        if(StringUtils.isNotEmpty(searchKey)){
            criteria = criteria.orOperator(
                    Criteria.where("usercode").regex(".*?"+searchKey+".*"),
                    Criteria.where("username").regex(".*?"+searchKey+".*"),
                    Criteria.where("url").regex(".*?"+searchKey+".*"),
                    Criteria.where("function").regex(".*?"+searchKey+".*")
            );
        }

        Query query = Query.query(criteria);

        List<Map> maps = mongoTemplate.find(query, Map.class, RECORD_COLLECTION_NAME);


        WorkbookContext workbook = WorkbookContextFactory.useXlsx().createWorkbook();

        SheetContext sheetCtx = workbook.createSheet("学生信息");

        sheetCtx
                .nextRow()
//                .skipCell()
                .header("id")
                .header("usercode")
                .header("username")
                .header("startTime")
                .header("timeMillis")
                .header("method")
                .header("controller")
                .header("endTime")
                .header("success")
                .header("url")
                .header("function")
                .header("requestParameters")
                .header("ip")
        ;

        for (int i=0;i<maps.size();i++){
            Map map = maps.get(i);
            sheetCtx
                    .nextRow()
                    .number(i + 1)
                    .text(getValue(map,"usercode"))
                    .text(getValue(map,"username"))
                    .text(getValue(map,"startTime"))
                    .text(getValue(map,"timeMillis"))
                    .text(getValue(map,"method"))
                    .text(getValue(map,"controller"))
                    .text(getValue(map,"endTime"))
                    .text(getValue(map,"success"))
                    .text(getValue(map,"url"))
                    .text(getValue(map,"function"))
                    .text(getValue(map,"requestParameters"))
                    .text(getValue(map,"ip"))
                    ;
        }

        byte[] bytes = workbook.toNativeBytes();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentLength(bytes.length);
        headers.setContentDispositionFormData("attachment", "record.xlsx");
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);

        ResponseEntity<byte[]> responseEntity = new ResponseEntity<byte[]>(bytes, headers, HttpStatus.OK);

        return responseEntity;
    }

    private String getValue(Map map,String key){
        Object v = map.get(key);
        if(v!=null){
            return String.valueOf(v);
        }else{
            return "";
        }

    }
}