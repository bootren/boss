package com.gitlab.bootren.boss.service;

import com.gitlab.bootren.boss.entity.Dic;
import com.gitlab.bootren.boss.repository.DicRepository;
import com.gitlab.bootren.common.utils.BeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * dic service
 */
@Service
@Transactional
public class DicServiceImpl {

    @Autowired
    DicRepository dicRepository;
    @Autowired
    private BeanMapper beanMapper;


    public Integer save(Dic dic) {
        Dic d = dic;
        if (dic.getId() != null) {
            d = findOne(dic.getId());
            beanMapper.map(dic, d);
        }
        dicRepository.save(d);
        return d.getId();
    }

    public void delete(Integer id) {
        Dic dic = dicRepository.findOne(id);
        if (dic != null) {
            dic.setDeleted(true);
            dicRepository.save(dic);
        }
    }

    public List<Dic> getDicList(String code) {
        return dicRepository.findAll("deleted = false and code = ?", code);
    }

    public Page<Dic> findPage(Pageable pageable, String code) {
        return dicRepository.findAll("deleted = false and code = ?", pageable, code);
    }

    public List<Dic> findGroups() {
        Map<String, Boolean> map = new HashMap<String, Boolean>();
        List<Dic> list = dicRepository.findAll("deleted = false ");
        List<Dic> l = new ArrayList<Dic>();
        for (Dic dic : list) {
            if (map.get(dic.getCode()) == null) {
                map.put(dic.getCode(), true);
                l.add(dic);
            }
        }
        return l;
    }

    public Dic findOne(Integer id) {
        return dicRepository.findOne(id);
    }
}