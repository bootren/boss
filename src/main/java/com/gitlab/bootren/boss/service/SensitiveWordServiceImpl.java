package com.gitlab.bootren.boss.service;

import com.gitlab.bootren.boss.entity.SensitiveWord;
import com.gitlab.bootren.boss.repository.SensitiveWordRepository;
import com.gitlab.bootren.common.utils.BeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 职位分类service
 * Created by LiYonghui on 2015/6/26.
 */
@Service
@Transactional
public class SensitiveWordServiceImpl implements SensitiveWordService {
    @Autowired
    SensitiveWordRepository sensitiveWordRepository;
    @Autowired
    private BeanMapper beanMapper;
    private List<SensitiveWord> seneitive;
    private List<SensitiveWord> sensitive;

//    @Override
//    public SensitiveWordVo getVo(Integer id) {
//        return beanMapper.map(getSensitiveWord(id), SensitiveWordVo.class);
//    }
//
//    @Override
//    public PageVo<SensitiveWordVo> findPageVo(PageableVo pageableVo, Map<String, Object> params) {
//        Page<SensitiveWord> page = findPage(PageConverterUtils.convert2PageRequest(pageableVo), params);
//        return PageConverterUtils.convert2PageVo(page, SensitiveWord.class, SensitiveWordVo.class);
//    }
//
//    @Override
//    public Integer saveVo(SensitiveWordVo sensitiveWordVo) {
//        return save(beanMapper.map(sensitiveWordVo, SensitiveWord.class));
//    }
//
//    @Override
//    public void deleteVo(Integer id) {
//        delete(id);
//    }
//
//    @Override
//    public List<SensitiveWordVo> findChildren(Integer parentId) {
//        return PageConverterUtils.convert2ListVo(findChildrenByParentId(parentId), SensitiveWord.class, SensitiveWordVo.class);
//    }
//
//    @Override
//    public List<SensitiveWordVo> findAll() {
//        return PageConverterUtils.convert2ListVo(findAllPosition(), SensitiveWord.class, SensitiveWordVo.class);
//    }

    public SensitiveWord getSensitiveWord(Integer id) {
        return sensitiveWordRepository.findOne(id);
    }

    public Integer save(SensitiveWord sensitiveWord) {
        SensitiveWord s = sensitiveWord;
        if (sensitiveWord.getId() != null) {
            s = getSensitiveWord(sensitiveWord.getId());
            beanMapper.map(sensitiveWord, s);
        }
        sensitiveWordRepository.save(s);
        return s.getId();
    }

    public void delete(Integer id) {
        SensitiveWord s = getSensitiveWord(id);
        if (s != null) {
            s.setDeleted(true);
            sensitiveWordRepository.save(s);
        }
    }

    public Page<SensitiveWord> findPage(Pageable pageable, Map<String, Object> params) {
        return sensitiveWordRepository.findAll("deleted = false", pageable);
    }

    public List<SensitiveWord> findChildrenByParentId(Integer parentId) {
        return sensitiveWordRepository.findAll("deleted = false and parentId = ?", parentId);
    }

    public List<SensitiveWord> findAllPosition() {
        return sensitiveWordRepository.findAll("deleted = false");
    }


//    public String replace(String arg,String name,String th) {
//        String arg0;
//        arg0 = arg.replaceAll(" ", "");
//        boolean flag = arg0.contains(name);
//        if (flag) {
//            for (int i = 0; i <= arg0.length() - 1; i++) {
//                for (int j = i; j <= arg0.length() - 1; j++) {
//                    String arg1 = arg0.substring(i, j);
//                    if (arg1.equals(name)) {
//                        String arg2 = arg0.replace(arg1, th);
//                        return arg2;
//                    }
//                }
//            }
//        }
//        return arg0;
//    }


    public String loopreplace(String args) {
        List<SensitiveWord> list = new ArrayList<SensitiveWord>();
        list = getSensitive();
        list.size();
        String arg = args;
        String th = "*";
        for (int q = 0; q <= list.size()-1; q++) {
            SensitiveWord sensitiveWord = list.get(q);
            String Name = sensitiveWord.getName();
            String arg0;
            arg0 = arg.replaceAll(" ", "");
            boolean flag = arg0.contains(Name);
            if (flag) {
                for (int i = 0; i <= arg0.length() - 1; i++) {
                    for (int j = i; j <= arg0.length() - 1; j++) {
                        String arg1 = arg0.substring(i, j);
                        if (arg1.equals(Name)) {
                            String arg2 = arg0.replace(arg1, th);
                            return arg2;
                        }
                    }
                }
            }
        }

        return args;
    }

    public List<SensitiveWord> getSensitive() {
        return sensitiveWordRepository.findAll("deleted = false");
    }
}
