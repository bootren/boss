package com.gitlab.bootren.boss.service;

import com.gitlab.bootren.boss.entity.DeviceApp;
import com.gitlab.bootren.boss.entity.OpenApp;
import com.gitlab.bootren.boss.repository.DeviceAppRepository;
import com.gitlab.bootren.boss.repository.OpenAppRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by serv on 15/10/15.
 */
@Service
@Transactional
public class OpenAppServiceImpl implements OpenAppService{

    private final static String OPEN_APP_PREFIX = "OPEN_APP:";


    @Autowired
    OpenAppRepository openAppRepository;
    @Autowired
    DeviceAppRepository deviceAppRepository;
    @Autowired
    StringRedisTemplate redisTemplate;

    // 注意： 新增、编辑和删除操作需要与数据库进行同步
    @Override
    public String getAppSecret(String appId){

        String appSecret = redisTemplate.boundValueOps(OPEN_APP_PREFIX + appId).get();
        if(StringUtils.isEmpty(appSecret)){
            OpenApp one = openAppRepository.findOne("appId = ?1", appId);
            if(one==null){
                throw new RuntimeException("错误的appId");
            }
            if(one.getDeleted()){
                throw new RuntimeException("该应用已经删除");
            }
            appSecret = one.getAppSecret();
            redisTemplate.boundValueOps(OPEN_APP_PREFIX+appId).set(appSecret);
        }
        return appSecret;
    }


    /**
     * 根据appId获取对应的app安装包对象
     * @param appId
     * @return
     */
    public DeviceApp findDeviceAppByAppId(String appId){
        DeviceApp one = deviceAppRepository.findOne("appId = ?1", appId);
        return one;
    }

    /**
     * 保存appId对应的版本号和下载地址
     * @param appId
     * @param appVersion
     * @param appUrl
     */
    public void saveDeviceApp(String appId,String appVersion,String appUrl){
        List<DeviceApp> all = deviceAppRepository.findAll("appId = ?1", appId);
        deviceAppRepository.delete(all);

        DeviceApp app = new DeviceApp();
        app.setAppId(appId);
        app.setAppUrl(appUrl);
        app.setAppVersion(appVersion);
        deviceAppRepository.save(app);
    }


}
