package com.gitlab.bootren.boss.service;

import com.gitlab.bootren.boss.entity.Area;
import com.gitlab.bootren.boss.repository.AreaRepository;
import com.gitlab.bootren.common.utils.BeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;

/**
 * area service
 */
@Service
@Transactional
public class AreaServiceImpl {

    @Autowired
    AreaRepository areaRepository;
    @Autowired
    private BeanMapper beanMapper;

    /********************************
     * 对外开放的服务实现
     *******************************/

//    @Override
//    public AreaVo getVo(Integer id) {
//        return beanMapper.map(getArea(id), AreaVo.class);
//    }
//
//    @Override
//    public PageVo<AreaVo> findPageVo(PageableVo pageableVo, Map<String, Object> params) {
//        Page<Area> page = findPage(PageConverterUtils.convert2PageRequest(pageableVo), params);
//        return PageConverterUtils.convert2PageVo(page, Area.class, AreaVo.class);
//    }
//
//    @Override
//    public Integer saveVo(AreaVo areaVo) {
//        return save(beanMapper.map(areaVo, Area.class));
//    }
//
//    @Override
//    public boolean deleteVo(Integer id) {
//        return delete(id);
//    }
//
//    @Override
//    public List<AreaVo> findChildren(Integer parentId) {
//        return PageConverterUtils.convert2ListVo(findChildrenByParentId(parentId), Area.class, AreaVo.class);
//    }
//
//    @Override
//    public List<AreaVo> findAll() {
//        return PageConverterUtils.convert2ListVo(findAllArea(), Area.class, AreaVo.class);
//    }

    /*****************************  BOSS 内部实现     *************************************/

    /**
     * 根据id 查找区域对象
     *
     * @param id
     * @return
     */
    public Area getArea(Integer id) {
        return areaRepository.findOne(id);
    }

    /**
     * 保存Area
     *
     * @param area
     */
    public Integer save(Area area) {
        Area a = area;
        if (area.getId() != null) {
            a = getArea(area.getId());
            beanMapper.map(area, a);
        }
        areaRepository.save(a);
        return a.getId();
    }

    /**
     * 根据id来删除area
     *
     * @param id
     */
    public boolean delete(Integer id) {
        Area a = getArea(id);
        if (a != null) {
            List<Area> list = findChildrenByParentId(a.getId());
            if (CollectionUtils.isEmpty(list)) {
                a.setDeleted(true);
                areaRepository.save(a);
                return true;
            }
        }
        return false;
    }

    /**
     * 获取热门城市（备用）
     *
     * @return
     */
    public List<Area> findHotCity() {
        return areaRepository.findAll("deleted = false and hotcity = ?", "1");
    }

    /**
     * 获取区域Page
     *
     * @param pageable
     * @param params
     * @return
     */
    public Page<Area> findPage(Pageable pageable, Map<String, Object> params) {
        return areaRepository.findAll("deleted = false", pageable);
    }

    /**
     * 由上级的parentId来获得Area对象
     *
     * @param parentId
     * @return
     */
    public List<Area> findChildrenByParentId(Integer parentId) {
        return areaRepository.findAll("deleted = false and parentId = ?", parentId);
    }

    /**
     * 查询所有的area对象
     *
     * @return
     */
    public List<Area> findAllArea() {
        return areaRepository.findAll("deleted = false", new Sort(new Sort.Order(Sort.Direction.ASC, "sort")));
    }
}