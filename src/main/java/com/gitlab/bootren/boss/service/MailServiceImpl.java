package com.gitlab.bootren.boss.service;

import com.gitlab.bootren.boss.vo.MailFileVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;

/**
 * Created by serv on 2015/7/28.
 */
@Service
public class MailServiceImpl implements MailService {

    @Autowired
    JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    String from;

    @Override
    @Deprecated
    public void send(String[] to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from);
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        javaMailSender.send(message);
    }

    // @see to : http://www.ietf.org/rfc/rfc822.txt
    @Override
    public void send(String to, String subject,String text) {
        send(to,subject,text,null,null);
    }

    @Override
    public void send(String to, String subject, String html, MailFileVo[] inlines, MailFileVo[] attachments) {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            //创建MimeMessageHelper对象，处理MimeMessage的辅助类
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setFrom(new InternetAddress(from, "同学派"));
            helper.setTo(InternetAddress.parse(to));
            helper.setSubject(subject);
            helper.setText(html,true);
            //添加资源文件
            if(inlines!=null){
                for (MailFileVo inlineFile : inlines){
                    helper.addInline(inlineFile.getFilename(), new ByteArrayResource(inlineFile.getData()),inlineFile.getContentType());
                }
            }
            //添加附件
            if(attachments!=null){
                for (MailFileVo attachmentFile : attachments){
                    helper.addAttachment(attachmentFile.getFilename(), new ByteArrayResource(attachmentFile.getData()), attachmentFile.getContentType());
                }
            }

        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        javaMailSender.send(mimeMessage);
    }
}
