package com.gitlab.bootren.boss.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * 分类
 */
public class Category{

    /**
     * 编号
     */
    private String code;

    /**
     * 类别名称
     */
    private String name;

    /**
     * 类型
     */
    private String type;

    /**
     * 子孙
     */
    private List<Category> children;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Category> getChildren() {
        return children;
    }

    public void setChildren(List<Category> children) {
        this.children = children;
    }

    public void addChildren(Category category){
        if(children==null){
            children = new ArrayList<Category>();
        }
        children.add(category);
    }
}
