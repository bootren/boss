package com.gitlab.bootren.boss.demo;

import com.gitlab.bootren.boss.Application;
import com.gitlab.bootren.boss.entity.SensitiveWord;
import com.gitlab.bootren.boss.service.SensitiveWordServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by  ZhangJiading on 2015/7/8.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class DemoTest {

//    @Autowired
//    IndustryServiceImpl industryService;

//    @Test
//    public void add() {
//        Industry industry = new Industry();
//        industry.setName("中国河南郑州工程师");
//        industry.setId(2);
//
//        industryService.saveVo(industry);
//        System.out.println("**************************" + industry.getId() + "**************************");
//        System.out.println("**************************" +industryService.findChildrenByParentId(2)+ "**************************");
//}

//    public void find() {
//        Industry i = new Industry();
//        i.setName("中国最好郑州");
//        i.setId(5);
//        industryService.save(i);
//        System.out.println("**************************" + industryService.findAllIndustry().get(5)+ "**************************");
////        System.out.println("**************************" + industryService.findIndustryById(5).getId() + "**************************");
////       industryService.delete(5);
////        System.out.println("**************************" + industryService.findIndustryById(5).getName() + "**************************");
//    }

//    @Autowired
//    SuggestionServiceImpl suggestionService;
//    @Test
//    public void add(){
//        Suggestion suggestion = new Suggestion();
//        suggestion.setId(1);
//        suggestion.setName("标题：客服建议");
//        suggestion.setContent("建议内容：河南先度科技。。。。。。。。。。。。。。。。。。。。。。。。");
//        suggestion.setReply("客服反馈内容：你好。。。。。。。。。。。。。。。。。。。。。。。。。。。。");
//        suggestionService.save(suggestion);
//        System.out.println("*************************" + suggestion.getId() + "*******************************");
//        System.out.println("*************************" + suggestionService.findSuggestionById(1).getName() + "****************************");
//        System.out.println("*************************" + suggestionService.findSuggestionById(1).getContent() + "****************************");
//        System.out.println("*************************" + suggestionService.findSuggestionById(1).getReply() + "****************************");

//    }

    @Autowired
    SensitiveWordServiceImpl sensitiveWordService;
    @Test
    public void ttt(){
        SensitiveWord sensitiveWord = new SensitiveWord();
//        sensitiveWord.setName("河南");
//        sensitiveWordService.save(sensitiveWord);
//        sensitiveWordService.replace("郑州人", "郑州", "#");
        System.out.println(sensitiveWordService.loopreplace("郑州河南"));
//        System.out.println(sensitiveWordService.replace("我爱郑州", "郑州", "#"));

    }
}
