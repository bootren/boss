package com.gitlab.bootren.boss.mail;

import com.aliyun.oss.internal.Mimetypes;
import com.gitlab.bootren.boss.Application;
import com.gitlab.bootren.boss.service.MailService;
import com.gitlab.bootren.boss.vo.MailFileVo;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;

/**
 * Created by serv on 2015/7/28.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class MailTest {

    @Autowired
    MailService mailService;

    @Test
    public void sendMail(){
        mailService.send(new String[]{"40409439@qq.com"},"注册成功","注册成功，请登录!");
    }
    @Test
    public void sendMail2(){
        mailService.send("40409439@qq.com","注册成功","注册成功，请登录!");
    }

    @Test
    public void sendHtml() throws IOException {

        //加载文件资源，作为附件
        ClassPathResource file = new ClassPathResource("mail/img/later_03.png");
        ClassPathResource file2 = new ClassPathResource("mail/img/yz_03.png");

        MailFileVo fileVo = new MailFileVo();
        fileVo.setFilename(file.getFilename());
        fileVo.setContentType(Mimetypes.getInstance().getMimetype(file.getFilename()));
        fileVo.setData(FileCopyUtils.copyToByteArray(file.getInputStream()));


        MailFileVo fileVo2 = new MailFileVo();
        fileVo2.setFilename(file2.getFilename());
        fileVo2.setContentType(Mimetypes.getInstance().getMimetype(file2.getFilename()));
        fileVo2.setData(FileCopyUtils.copyToByteArray(file2.getInputStream()));

        mailService.send("40409439@qq.com","您正在注册同学派，请激活您的账号!", FileUtils.readFileToString(new ClassPathResource("mail/email.html").getFile()),new MailFileVo[]{fileVo,fileVo2},null);
    }
}
