package com.gitlab.bootren.boss;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.bootren.boss.entity.Area;
import com.gitlab.bootren.boss.entity.OpenApp;
import com.gitlab.bootren.boss.repository.AreaRepository;
import com.gitlab.bootren.boss.repository.OpenAppRepository;
import com.gitlab.bootren.code.service.CodeService;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by serv on 15/10/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class CodeTest {


    @Autowired
    CodeService codeService;

    @Autowired
    OpenAppRepository openAppRepository;
    @Autowired
    AreaRepository areaRepository;


    @Test
    public void insertIOS(){
        String appId = codeService.getCode(OpenApp.class.getName(), "TXP");
        String appSecret = RandomStringUtils.randomAlphanumeric(32);
        OpenApp openApp = new OpenApp();
        openApp.setName("ios_app");
        openApp.setDescription("ios客户端应用");
        openApp.setLogo("http://p1.tongxuepie.com/upload/image/20151015/1444887380802010018.png");
        openApp.setUrl("http://tongxuepie.com");
        openApp.setAppId(appId);
        openApp.setAppSecret(appSecret);
        openAppRepository.save(openApp);
    }

    @Test
    public void insertAndroid(){
        String appId = codeService.getCode(OpenApp.class.getName(), "TXP");
        String appSecret = RandomStringUtils.randomAlphanumeric(32);
        OpenApp openApp = new OpenApp();
        openApp.setName("android_app");
        openApp.setDescription("android客户端应用");
        openApp.setLogo("http://p1.tongxuepie.com/upload/image/20151015/1444887380802010018.png");
        openApp.setUrl("http://tongxuepie.com");
        openApp.setAppId(appId);
        openApp.setAppSecret(appSecret);
        openAppRepository.save(openApp);
    }

    @Test
    public void insertRollCall(){
        String appId = codeService.getCode(OpenApp.class.getName(), "TXP");
        String appSecret = RandomStringUtils.randomAlphanumeric(32);
        OpenApp openApp = new OpenApp();
        openApp.setName("rollCall_app");
        openApp.setDescription("点名应用");
        openApp.setLogo("http://p1.tongxuepie.com/upload/image/20151015/1444887380802010018.png");
        openApp.setUrl("http://tongxuepie.com");
        openApp.setAppId(appId);
        openApp.setAppSecret(appSecret);
        openAppRepository.save(openApp);
    }

    @Test
    public void testArea() throws JsonProcessingException {
        List<Map> maps = new ArrayList<Map>();
        List<Area> all = areaRepository.findAll("parentId = 0");
        for (Area area : all){
            Map map = new HashMap();
            map.put("id",area.getId());
            map.put("name",area.getName());
            map.put("children", Lists.transform(areaRepository.findAll("parentId = ?1", area.getId()), new Function<Area, Map>() {
                @Override
                public Map apply(Area input) {
                    Map map = new HashMap();
                    map.put("id",input.getId());
                    map.put("name",input.getName());
                    return map;
                }
            }));
            maps.add(map);
        }
        System.out.println(new ObjectMapper().writeValueAsString(maps));
    }

}
