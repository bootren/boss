package com.gitlab.bootren.boss;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.bootren.aliyun.oss.FileClientService;
import com.gitlab.bootren.aliyun.oss.FileResult;
import com.gitlab.bootren.boss.entity.Category;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by serv on 15/9/23.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class CategoryTest {


    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    FileClientService fileClientService;


    /**
     * 参考数据 http://core.pc.lietou-static.com/revs/js/common/plugins/localdata/job_98536572.js
     * @throws Exception
     */
    @Test
    public void writeJs() throws Exception {
        wirteJs("zh_CN");
        wirteJs("en_US");
    }


    private void wirteJs(String locale) throws Exception {
        List<Category> all = getData(locale);
        String s = "var category_"+locale+" = ";
        s += objectMapper.writeValueAsString(all);
        FileResult fileResult = fileClientService.saveFile(s.getBytes(Charset.forName("utf-8")),"boss/category/"+locale+System.currentTimeMillis()+".js");
        System.out.println(fileResult.getUrl());
    }

    private List<Category> getData(String locale) throws IOException {

        List<Category> categories = new ArrayList<Category>();

        LinkedHashMap<String,List<String>> map = objectMapper.readValue(new ClassPathResource("data.json").getFile(), LinkedHashMap.class);

        Iterator<String> iterator = map.keySet().iterator();
        while (iterator.hasNext()){
            String key = iterator.next();
            Category category = new Category();
            category.setCode(key);
            category.setName(locale.equals("zh_CN") ? map.get(key).get(0) : map.get(key).get(1));
            if(key.contains("cate")){
                category.setType("industry");
            }else{
                category.setType("position");
            }
            categories.add(category);
        }

        LinkedHashMap<String,List<String>> relationMap = objectMapper.readValue(new ClassPathResource("relation.json").getFile(), LinkedHashMap.class);

        Iterator<String> it = relationMap.keySet().iterator();
        while (it.hasNext()){
            String key = it.next();
            List<String> list = relationMap.get(key);

            Category parent = getCategory(categories,key);

            for (String code : list){
                parent.addChildren(getCategory(categories,code));
            }
            if(key.split("-").length>2){
                //["530", "531", "532", "533", "534", "hot-06", "539", "540", "541"]
                parent.addChildren(getCategory(categories,"530"));
                parent.addChildren(getCategory(categories,"531"));
                parent.addChildren(getCategory(categories,"532"));
                parent.addChildren(getCategory(categories,"533"));
                parent.addChildren(getCategory(categories,"534"));
                parent.addChildren(getCategory(categories,"hot-06"));
                parent.addChildren(getCategory(categories,"539"));
                parent.addChildren(getCategory(categories,"540"));
                parent.addChildren(getCategory(categories,"541"));
            }
        }


        List<Category> dataList = new ArrayList<Category>();
        dataList.add(getCategory(categories,"cate-01"));
        dataList.add(getCategory(categories,"cate-02"));
        dataList.add(getCategory(categories,"cate-03"));
        dataList.add(getCategory(categories,"cate-04"));
        dataList.add(getCategory(categories,"cate-05"));
        dataList.add(getCategory(categories,"cate-06"));
        dataList.add(getCategory(categories,"cate-07"));
        dataList.add(getCategory(categories,"cate-08"));
        dataList.add(getCategory(categories,"cate-09"));
        dataList.add(getCategory(categories,"cate-10"));
        dataList.add(getCategory(categories,"cate-11"));
        dataList.add(getCategory(categories,"cate-12"));

        return dataList;
    }


    private Category getCategory(List<Category> categories,String code){
        for (Category category : categories){
            if(category.getCode().equals(code)){
                return category;
            }
        }
        return null;
    }


}
